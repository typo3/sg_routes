## Version 7 Breaking Changes

- Dropped TYPO3 11 support
- Dropped PHP 7 support
- Removed PageNotFoundHandlingUpdate wizard
- Removed IconViewHelper in favor of core icon view helper
- Removed AddJavaScriptFileViewHelper in favor of f:asset
- Changed JavaScript files from requireJS to JavaScript module

## Version 6 Breaking Changes

- Dropped TYPO3 9+10 support
- PageNotFound Handling Code and BE Module got removed
- Moved over to using sys_redirect records. Please run the update wizard after DB Wizard
- Dropped php 7.3 support

## Version 4 Breaking Changes

- Dropped TYPO3 8 support
- Pagenotfound Handling got removed, because it does no longer work with TYPO3 10. Please use SiteConfiguration and use
  the PageNotFoundHandling upgrade wizard if your installation has old PageNotFoundHandlings configured.
