/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
import DocumentService from '@typo3/core/document-service.js';
import ModuleMenu from '@typo3/backend/module-menu.js';
import Viewport from '@typo3/backend/viewport.js';
import DateTimePicker from '@typo3/backend/date-time-picker.js';

const SgRoutes = {
	init: function() {
		document.querySelectorAll('.btn-delete-all').forEach((button) => {
			button.addEventListener('click', SgRoutes.deleteAllListener);
		});
		const filterResetButton = document.getElementById('filter-reset-btn');
		if (filterResetButton) {
			filterResetButton.addEventListener(
				'click',
				function(event) {
					event.preventDefault();
					this.form.reset();
					this.closest('form').querySelectorAll('select').forEach((select) => {
						// reset all selects to their default value (even the ones with multiple="multiple")
						select.selectedIndex = -1;
					});
					document.querySelectorAll('.reset-me').forEach((reset) => {
						reset.value = '';
					});
					this.form.submit();
				}
			);
		}

		document.querySelectorAll('.sg-routes_pageswitch').forEach((button) => {
			button.addEventListener('click', (event) => {
				if (Viewport.NavigationContainer.PageTree !== undefined) {
					event.preventDefault();
					SgRoutes.goTo('web_SgRoutesRoute', event.target.dataset.page);
				}
			});
		});

		const paginators = document.querySelectorAll('.paginator-input');
		for (const paginator of paginators) {
			paginator.addEventListener('keypress', (event) => {
				if (event.keyCode === 13) {
					event.preventDefault();
					const page = event.currentTarget.value;
					const url = event.currentTarget.dataset.uri;
					self.location.href = url.replace('987654321', page);
				}
			})
		}

		document.querySelectorAll('.t3js-datetimepicker').forEach(function(element) {
			DateTimePicker.initialize(element);
		});
	},
	/**
	 * Deletes all routes
	 *
	 * @param _event
	 */
	deleteAllListener: function(_event) {
		_event.preventDefault();

		const confirm = TYPO3.lang['backend.delete_all'];
		if (window.confirm(confirm)) {
			window.location = _event.currentTarget.href;
		}
	},
	/**
	 * opens the selected category edit form
	 *
	 * @return {boolean}
	 */
	editSelectedCategory: function() {
		const selected = document.getElementById('filter-categories').value;
		if (selected) {
			const editLink = document.getElementById('sg-routes-categoryeditlinks-' + selected[0]);
			if (editLink) {
				window.location.href = editLink.dataset.url + '&returnUrl=' + encodeURIComponent(window.location.pathname + window.location.search);
			}
		}
	},
	goTo: function(module, id) {
		const pageTreeNodes = Viewport.NavigationContainer.PageTree.instance.nodes;
		for (let nodeIndex in pageTreeNodes) {
			if (pageTreeNodes.hasOwnProperty(nodeIndex) && pageTreeNodes[nodeIndex].identifier === parseInt(id)) {
				Viewport.NavigationContainer.PageTree.selectNode(pageTreeNodes[nodeIndex]);
				break;
			}
		}

		ModuleMenu.App.showModule(module, 'id=' + id);
	}
};

DocumentService.ready().then(() => {
	SgRoutes.init();
});
