<?php

use SGalinski\SgRoutes\Event\Listener\AfterAutoCreateRedirectHasBeenPersistedEventListener;
use SGalinski\SgRoutes\Event\Listener\AfterBackendPageRenderEventListener;
use SGalinski\SgRoutes\Event\Listener\ExcludeFromReferenceIndexListener;
use SGalinski\SgRoutes\Event\Listener\ModifyPageLayoutContentEventListener;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use TYPO3\CMS\Backend\Controller\Event\AfterBackendPageRenderEvent;
use TYPO3\CMS\Backend\Controller\Event\ModifyPageLayoutContentEvent;
use TYPO3\CMS\Core\DataHandling\Event\IsTableExcludedFromReferenceIndexEvent;
use TYPO3\CMS\Redirects\Event\AfterAutoCreateRedirectHasBeenPersistedEvent;

return static function (ContainerConfigurator $containerConfigurator): void {
	$services = $containerConfigurator->services();

	$services->defaults()
		->private()
		->autowire()
		->autoconfigure();
	$services->load('SGalinski\\SgRoutes\\', __DIR__ . '/../Classes/');

	$services->set(ExcludeFromReferenceIndexListener::class)
		->tag('event.listener', [
			'identifier' => 'sg-routes/excludeFromRefIndex',
			'event' => IsTableExcludedFromReferenceIndexEvent::class
		]);
	$services->set(AfterAutoCreateRedirectHasBeenPersistedEventListener::class)
		->tag('event.listener', [ 'event' => AfterAutoCreateRedirectHasBeenPersistedEvent::class]);
	$services->set(ModifyPageLayoutContentEventListener::class)
		->tag('event.listener', [ 'event' => ModifyPageLayoutContentEvent::class]);
	$services->set(AfterBackendPageRenderEventListener::class)
		->tag('event.listener', ['event' => AfterBackendPageRenderEvent::class]);
};
