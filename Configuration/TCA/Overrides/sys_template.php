<?php

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

// Adds the static TypoScript template.
ExtensionManagementUtility::addStaticFile(
	'sg_routes',
	'Configuration/TypoScript/Frontend',
	'SgRoutes - Configuration'
);
