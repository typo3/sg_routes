<?php

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

ExtensionManagementUtility::addTCAcolumns(
	'sys_redirect',
	[
		'source_url_case_sensitive' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:sys_redirect.source_url_case_sensitive',
			'description' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:sys_redirect.source_url_case_sensitive.description',
			'config' => [
				'type' => 'check',
				'default' => '0'
			],
			'displayCond' => 'FIELD:use_regular_expression:!=:1'
		],
		'destination_language' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:sys_redirect.destination_language',
			'description' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:sys_redirect.destination_language.description',
			'config' => [
				'type' => 'language'
			]
		],
		'description' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:sys_redirect.description',
			'config' => [
				'type' => 'text',
				'eval' => 'trim'
			],
		],
		'priority' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:sys_redirect.priority',
			'config' => [
				'type' => 'number'
			],
		],
		'categories' => [
			'exclude' => 1,
			'label' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:sys_redirect.categories',
			'config' => [
				'type' => 'select',
				'autoSizeMax' => 50,
				'maxitems' => 9999,
				'size' => 10,
				'foreign_table' => 'tx_sgroutes_domain_model_category',
				'foreign_table_where' => 'AND tx_sgroutes_domain_model_category.pid = ###CURRENT_PID###',
				'MM' => 'tx_sgroutes_domain_model_redirect_category',
				'renderType' => 'selectMultipleSideBySide',
				'fieldControl' => [
					'addRecord' => [
						'disabled' => FALSE
					],
					'editPopup' => [
						'disabled' => FALSE
					]
				]
			],
		],
		'regular_expression_info' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:sys_redirect.regular_expression_info',
			'config' => [
				'type' => 'user',
				'renderType' => 'SgRoutesTCAInfoField',
				'userFunc' => 'SGalinski\\SgRoutes\\Backend\\TCAInfoField->render',
				'parameters' => [
					'links' => [
						[
							'https://www.cheatography.com/davechild/cheat-sheets/regular-expressions/',
							'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:sys_redirect.regular_expression_info_cheatsheet'
						],
						[
							'https://regex101.com/',
							'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:sys_redirect.regular_expression_info_tester'
						]
					]
				]
			],
		],
		'regular_expression_replace_pattern' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:sys_redirect.regular_expression_replace_pattern',
			'config' => [
				'type' => 'input',
				'eval' => 'trim'
			],
		],
		'createdon' => [
			'label' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:sys_redirect.crdate',
			'config' => [
				'type' => 'passthrough'
			],
		],
		'updatedon' => [
			'label' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:sys_redirect.tstamp',
			'config' => [
				'type' => 'passthrough'
			],
		],
	]
);

ExtensionManagementUtility::addToAllTCAtypes(
	'sys_redirect',
	'regular_expression_info,regular_expression_replace_pattern,priority,source_url_case_sensitive,destination_language',
	1,
	'after:is_regexp'
);

ExtensionManagementUtility::addToAllTCAtypes(
	'sys_redirect',
	'description,categories',
	1,
	'after:protected'
);

$GLOBALS['TCA']['sys_redirect']['ctrl']['security'] = [
	'ignorePageTypeRestriction' => TRUE
];
