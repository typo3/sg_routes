<?php

$columns = [
	'ctrl' => [
		'title' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:tx_sgroutes_domain_model_log',
		'label' => 'request_url',
		'crdate' => 'crdate',
		'searchFields' => 'source_url, destination_url, redirect_code, description, categories, request_url, redirect_url',
		'hideTable' => TRUE,
		'iconfile' => 'EXT:sg_routes/Resources/Public/Icons/sys_redirect.svg',
		'security' => [
			'ignorePageTypeRestriction' => TRUE
		]
	],
	'interface' => [],
	'types' => [
		'1' => [
			'showitem' => 'use_regular_expression, source_url, destination_url, redirect_url_parameters, redirect_code, description, categories, request_url, redirect_url, execution_duration',
		],
	],
	'columns' => [
		'crdate' => [
			'label' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:tx_sgroutes_domain_model_route_hit.crdate',
			'config' => [
				'type' => 'passthrough'
			],
		],
		'use_regular_expression' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:sys_redirect.use_regular_expression',
			'config' => [
				'type' => 'check'
			],
		],
		'source_url' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:tx_sgroutes_domain_model_log.source_url',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'source_host' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:tx_sgroutes_domain_model_log.source_host',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'ip_address' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:tx_sgroutes_domain_model_log.ip_address',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'destination_url' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:tx_sgroutes_domain_model_log.destination_url',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'redirect_url_parameters' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:sys_redirect.redirect_url_parameters',
			'config' => [
				'type' => 'check'
			],
		],
		'redirect_code' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:sys_redirect.redirect_code',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectSingle',
				'size' => 1,
				'items' => [
					['label' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:sys_redirect.permanently', 'value' => '301'],
					['label' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:sys_redirect.temporary', 'value' => '302'],
					['label' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:sys_redirect.temporary307', 'value' => '307']
				]
			],
		],
		'description' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:sys_redirect.description',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'categories' => [
			'exclude' => 1,
			'label' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:sys_redirect.categories',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'request_url' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:tx_sgroutes_domain_model_log.request_url',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'redirect_url' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:tx_sgroutes_domain_model_log.redirect_url',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'execution_duration' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:tx_sgroutes_domain_model_log.execution_duration',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
	],
];

return $columns;
