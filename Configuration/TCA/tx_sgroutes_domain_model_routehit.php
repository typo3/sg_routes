<?php

return [
	'ctrl' => [
		'title' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:tx_sgroutes_domain_model_route_hit',
		'label' => 'crdate',
		'crdate' => 'crdate',
		'searchFields' => '',
		'hideTable' => TRUE,
		'iconfile' => 'EXT:sg_routes/Resources/Public/Icons/sys_redirect.svg'
	],
	'types' => [
		'1' => [
			'showitem' => 'route_uid, ip_address, crdate',
		],
	],
	'columns' => [
		'route_uid' => [
			'label' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:tx_sgroutes_domain_model_route_hit.route_uid',
			'config' => [
				'type' => 'passthrough'
			],
		],
		'crdate' => [
			'label' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:tx_sgroutes_domain_model_route_hit.crdate',
			'config' => [
				'type' => 'passthrough'
			],
		],
		'ip_address' => [
			'label' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:tx_sgroutes_domain_model_route_hit.ip_address',
			'readOnly' => TRUE,
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
	],
];
