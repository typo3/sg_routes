<?php

use SGalinski\SgRoutes\Domain\Model\Route;

return [
	Route::class => [
		'tableName' => 'sys_redirect',
		'properties' => [
			'useRegularExpression' => [
				'fieldName' => 'is_regexp'
			],
			'hits' => [
				'fieldName' => 'hitcount'
			],
			'lastHit' => [
				'fieldName' => 'lasthiton'
			],
			'redirectCode' => [
				'fieldName' => 'target_statuscode'
			],
			'sourceUrl' => [
				'fieldName' => 'source_path'
			],
			'destinationUrl' => [
				'fieldName' => 'target'
			]
		]
	],
];
