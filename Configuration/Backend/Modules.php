<?php

use SGalinski\SgRoutes\Controller\RouteController;

return [
	'web_sgroutes_route' => [
		'parent' => 'web',
		'position' => ['after' => ''],
		'access' => 'user',
		'workspaces' => 'live',
		'path' => '/module/web/sgroutes_route',
		'labels' => 'LLL:EXT:sg_routes/Resources/Private/Language/locallang.xlf',
		'iconIdentifier' => 'ext-sg_routes_module',
		'extensionName' => 'SgRoutes',
		'controllerActions' => [
			RouteController::class => [
				'list',
				'activateDemoMode',
				'update',
				'create',
				'delete',
				'deleteAll',
				'htaccess',
				'log',
				'pageNotFoundHandling',
				'export',
				'resetHits'
			]
		]
	]
];
