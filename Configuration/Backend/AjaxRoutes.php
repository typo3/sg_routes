<?php

use SGalinski\SgRoutes\Backend\Ajax;

return [
	'sg_routes::checkLicense' => [
		'path' => '/sg_routes/checkLicense',
		'target' => Ajax::class . '::checkLicense',
	],
];
