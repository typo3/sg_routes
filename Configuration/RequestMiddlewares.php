<?php

use SGalinski\SgRoutes\Middleware\RedirectResolver;

return [
	'frontend' => [
		'typo3/cms-redirects/redirecthandler' => [
			'disabled' => TRUE
		],
		'sgalinski/sg-routes/redirect-resolver' => [
			'target' => RedirectResolver::class,
			'before' => [
				'typo3/cms-frontend/base-redirect-resolver',
			],
			'after' => [
				'typo3/cms-frontend/authentication',
			],
		],
	],
];
