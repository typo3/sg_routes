<?php

use TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider;

/**
 * Important! Do not return a variable named $icons, because it will result in an error.
 * The core requires this file and then the variable names will clash.
 * Either use a closure here, or do not call your variable $icons.
 */

$iconList = [];
foreach (['ext-sg_routes_module' => 'module-sgroutes.svg'] as $identifier => $path) {
	$iconList[$identifier] = [
		'provider' => SvgIconProvider::class,
		'source' => 'EXT:sg_routes/Resources/Public/Icons/' . $path,
	];
}

return $iconList;
