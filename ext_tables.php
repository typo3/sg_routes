<?php

use SGalinski\SgRoutes\Controller\RouteController;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Extbase\Utility\ExtensionUtility;
use TYPO3\CMS\Scheduler\Task\TableGarbageCollectionTask;

call_user_func(
	static function () {
		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks'][TableGarbageCollectionTask::class]['options']['tables']['tx_sgroutes_domain_model_log'] = [
			'dateField' => 'crdate',
			'expirePeriod' => 360
		];

		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks'][TableGarbageCollectionTask::class]['options']['tables']['tx_sgroutes_domain_model_routehit'] = [
			'dateField' => 'crdate',
			'expirePeriod' => 360
		];
	}
);
