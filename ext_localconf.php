<?php

use SGalinski\SgRoutes\Backend\TCAInfoField;
use SGalinski\SgRoutes\Hook\LicenceCheckHook;
use SGalinski\SgRoutes\Hook\RouteDataHandlerHook;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

call_user_func(
	static function () {
		ExtensionManagementUtility::addTypoScriptConstants(
			'@import "EXT:sg_routes/Configuration/TypoScript/Backend/constants.typoscript"'
		);

		ExtensionManagementUtility::addTypoScriptSetup(
			'@import "EXT:sg_routes/Configuration/TypoScript/Backend/setup.typoscript"'
		);

		ExtensionManagementUtility::addUserTSConfig(
			'@import "EXT:sg_routes/Configuration/TsConfig/User/*.tsconfig"'
		);

		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass'][] =
			RouteDataHandlerHook::class;

		$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][] = [
			'nodeName' => 'SgRoutesTCAInfoField',
			'priority' => 40,
			'class' => TCAInfoField::class,
		];
	}
);
