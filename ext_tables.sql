CREATE TABLE sys_redirect (
	description                        text,
	categories                         int(11)             DEFAULT '0'  NOT NULL,
	source_url_case_sensitive          tinyint(4) unsigned DEFAULT '0'  NOT NULL,
	destination_language               int(11) unsigned    DEFAULT '0'  NOT NULL,
	regular_expression_replace_pattern varchar(255)        DEFAULT ''   NOT NULL,
	crdate                             int(11) unsigned    DEFAULT '0'  NOT NULL,
	tstamp                             int(11) unsigned    DEFAULT '0'  NOT NULL,
	priority                           int(11) unsigned    DEFAULT '10' NOT NULL
);

CREATE TABLE tx_sgroutes_domain_model_category (
	title                      varchar(255)        DEFAULT ''  NOT NULL,
	description                text,
	used_for_automated_routing tinyint(4) unsigned DEFAULT '0' NOT NULL
);

CREATE TABLE tx_sgroutes_domain_model_log (
	use_regular_expression  tinyint(4) unsigned DEFAULT '0'   NOT NULL,
	source_url              varchar(255)        DEFAULT ''    NOT NULL,
	source_host             varchar(255)        DEFAULT ''    NOT NULL,
	destination_url         varchar(255)        DEFAULT ''    NOT NULL,
	redirect_url_parameters tinyint(4) unsigned DEFAULT '0'   NOT NULL,
	redirect_code           varchar(255)        DEFAULT '301' NOT NULL,
	description             varchar(255)        DEFAULT ''    NOT NULL,
	categories              text,
	request_url             varchar(255)        DEFAULT ''    NOT NULL,
	redirect_url            varchar(255)        DEFAULT ''    NOT NULL,
	execution_duration      int(11)             DEFAULT '0'   NOT NULL,
	ip_address              varchar(255)        DEFAULT ''    NOT NULL
);

#
# !!!DO NOT RENAME!!!
# This table was intentionally called `tx_sgroutes_domain_model_routehit` instead of `tx_sgroutes_domain_model_route_hit`.
# The same is valid for the corresponding model, repository & TCA file.
# Since we update the route hits during a redirect, the typoscript that was needed to tell extbase that our table has
# another underscore in the name was not ready yet.
# Instead of continuing wasting time on debugging this, I chose to stick with this table name instead.
#
CREATE TABLE tx_sgroutes_domain_model_routehit (
	route_uid  int(11)          DEFAULT '0' NOT NULL,
	ip_address varchar(255)     DEFAULT ''  NOT NULL,
	crdate     int(11) unsigned DEFAULT '0' NOT NULL
);
