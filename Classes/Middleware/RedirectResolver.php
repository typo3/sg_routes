<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgRoutes\Middleware;

use Doctrine\DBAL\Exception;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use SGalinski\SgRoutes\Domain\Repository\RouteRepository;
use SGalinski\SgRoutes\Service\IpAnonymizerService;
use SGalinski\SgRoutes\Service\LicenceCheckService;
use TYPO3\CMS\Core\Configuration\SiteConfiguration;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Database\Query\Restriction\HiddenRestriction;
use TYPO3\CMS\Core\Error\Http\PageNotFoundException;
use TYPO3\CMS\Core\Http\RedirectResponse;
use TYPO3\CMS\Core\LinkHandling\Exception\UnknownLinkHandlerException;
use TYPO3\CMS\Core\LinkHandling\LinkService;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\Controller\ErrorController;
use TYPO3\CMS\Frontend\Page\PageAccessFailureReasons;

/**
 * Middleware resolver class for the sg_routes
 */
class RedirectResolver implements MiddlewareInterface {
	/**
	 * RedirectResolver constructor.
	 * This used to get our RouteRepository, but in TYPO3 v11 this
	 * crashed various Ajax calls with missing Controller/Actions in pluginConfiguration
	 * sometimes even the full Frontend. We use a local Objectmanager::get at the location instead
	 */
	public function __construct() {
	}

	/**
	 * Process an incoming server request.
	 *
	 * Processes an incoming server request in order to produce a response.
	 * If unable to produce the response itself, it may delegate to the provided
	 * request handler to do so.
	 *
	 * @param ServerRequestInterface $request
	 * @param RequestHandlerInterface $handler
	 * @return ResponseInterface
	 * @throws PageNotFoundException
	 * @throws Exception
	 */
	public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface {
		$testRequestUri = NULL;

		if (!LicenceCheckService::isInDevelopmentContext()
			&& !LicenceCheckService::hasValidLicense()
			&& !LicenceCheckService::isInDemoMode()) {
			return $handler->handle($request);
		}

		$start = \microtime(TRUE);
		$rootUid = $request->getAttribute('site')->getRootPageId();

		// get the first available site if no site is given (this allows support for redirect other domains
		// in non-multidomain instances)
		if ($rootUid <= 0) {
			$siteConfiguration = GeneralUtility::makeInstance(SiteConfiguration::class);
			$sites = $siteConfiguration->getAllExistingSites();
			/** @noinspection LoopWhichDoesNotLoopInspection */
			foreach ($sites as $site) {
				$rootUid = $site->getRootPageId();
				break;
			}
		}

		if ($rootUid <= 0 || (isset($GLOBALS['TYPO3_CONF_VARS']['FE']['tx_routes_executed']) &&
				array_key_exists($rootUid, $GLOBALS['TYPO3_CONF_VARS']['FE']['tx_routes_executed']) &&
				$GLOBALS['TYPO3_CONF_VARS']['FE']['tx_routes_executed'][$rootUid])
		) {
			// already redirected the route to $rootUid, proceed with page loading
			return $handler->handle($request);
		}

		$GLOBALS['TYPO3_CONF_VARS']['FE']['tx_routes_executed'][$rootUid] = TRUE;

		$serverParams = $request->getServerParams();
		$userIpAddress = $serverParams['REMOTE_ADDR'];
		$requestParameters = $request->getQueryParams();
		$filteredRequestParameters = [];
		foreach ($requestParameters as $key => $splittedParameter) {
			if ($key === 'id') {
				continue;
			}

			$filteredRequestParameters[$key] = $splittedParameter;
		}

		$requestParameters = $filteredRequestParameters;
		unset($filteredRequestParameters);

		$requestUriObject = clone $request->getUri();
		$requestUriObject = $requestUriObject->withPath(\trim($requestUriObject->getPath(), '/'));
		$requestUriObject2 = clone $requestUriObject;

		$requestUriWithParameters = (string) $requestUriObject;
		$requestUriObject = $requestUriObject->withQuery('');
		$requestUri = (string) $requestUriObject;
		$requestUriObject = $requestUriObject->withHost('')->withScheme('');
		$requestUriWithoutHost = (string) $requestUriObject;

		$requestUriObject2 = $requestUriObject2->withHost('')->withScheme('');
		$requestUriWithParametersWithoutHost = (string) $requestUriObject2;

		$requestUriLowercased = \mb_strtolower($requestUri);
		$requestUriWithoutHostLowercased = \mb_strtolower($requestUriWithoutHost);
		$requestUriWithoutHostWithParametersLowercased = \mb_strtolower($requestUriWithParametersWithoutHost);
		$requestUriWithParametersLowercased = \mb_strtolower($requestUriWithParameters);

		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
			->getQueryBuilderForTable('sys_redirect');
		$queryBuilder->getRestrictions()->removeAll()
			->add(GeneralUtility::makeInstance(DeletedRestriction::class))
			->add(GeneralUtility::makeInstance(HiddenRestriction::class));
		$resultArray = $queryBuilder->select(
			'uid',
			'pid',
			'is_regexp',
			'regular_expression_replace_pattern',
			'source_host',
			'source_path',
			'source_url_case_sensitive',
			'target',
			'destination_language',
			'respect_query_parameters',
			'keep_query_parameters',
			'target_statuscode',
			'description' // required for the later logging process
		)
			->from('sys_redirect')
			->where(
				$queryBuilder->expr()->in('pid', [$queryBuilder->createNamedParameter($rootUid, \PDO::PARAM_INT), 0]),
				$queryBuilder->expr()->or(
					$queryBuilder->expr()->eq('source_host', $queryBuilder->createNamedParameter('*')),
					$queryBuilder->expr()->eq(
						'source_host',
						$queryBuilder->createNamedParameter($_SERVER['HTTP_HOST'])
					)
				)
			)->orderBy('priority', 'ASC')->executeQuery()->fetchAllAssociative();

		if (!is_array($resultArray) || !count($resultArray)) {
			return $handler->handle($request);
		}

		$doRedirect = FALSE;
		$finalRedirect = NULL;
		foreach ($resultArray as $redirect) {
			if ($redirect['source_path'] === '' || $redirect['target'] === '') {
				continue;
			}

			if ($redirect['is_regexp']) {
				if ($redirect['respect_query_parameters']) {
					$testRequestUri = $requestUriWithParameters;
				} else {
					$testRequestUri = $requestUri;
				}

				$possibleDelimiter = $redirect['source_path'][0];
				if ($possibleDelimiter === $redirect['source_path'][strlen($redirect['source_path']) - 1]) {
					$redirect['source_path'] = trim($redirect['source_path'], $possibleDelimiter);
				}

				$pattern = '/' . \str_replace('/', '\\/', $redirect['source_path']) . '/is';
				if ($redirect['regular_expression_replace_pattern'] && \preg_match($pattern, $testRequestUri)) {
					$doRedirect = TRUE;
					$redirect['target'] = \preg_replace(
						$pattern,
						$redirect['regular_expression_replace_pattern'],
						$testRequestUri
					);
					$finalRedirect = $redirect;
					break;
				}

				if (\preg_match($pattern, $testRequestUri)) {
					$finalRedirect = $redirect;
					$doRedirect = TRUE;
					break;
				}
			} else {
				if ($redirect['respect_query_parameters']) {
					$testRequestUri = $requestUriWithParametersWithoutHost;
					$testRequestUriLowercased = $requestUriWithoutHostWithParametersLowercased;
				} else {
					$testRequestUri = $requestUriWithoutHost;
					$testRequestUriLowercased = $requestUriWithoutHostLowercased;
				}

				// remove trailing slash
				$sourceUrl = rtrim($redirect['source_path'], '/');

				// It's possible that you want to do an exact check including the domain.
				// E.g., to only allow a redirect for the development context that has a different domain
				if (str_contains($sourceUrl, 'http')) {
					if ($redirect['respect_query_parameters']) {
						$testRequestUri = $requestUriWithParameters;
						$testRequestUriLowercased = $requestUriWithParametersLowercased;
					} else {
						$testRequestUri = $requestUri;
						$testRequestUriLowercased = $requestUriLowercased;
					}
				}

				$isCaseSensitive = $redirect['source_url_case_sensitive'];
				if (
					($isCaseSensitive && $testRequestUri === $sourceUrl)
					|| (!$isCaseSensitive && $testRequestUriLowercased === \mb_strtolower($sourceUrl))
				) {
					$doRedirect = TRUE;
					$finalRedirect = $redirect;
					break;
				}
			}
		}

		if ($doRedirect && $finalRedirect !== NULL) {
			if (!$finalRedirect['keep_query_parameters']) {
				$requestParameters = [];
			}

			$redirectUri = $this->getRedirectUrl(
				$finalRedirect['target'],
				$requestParameters,
				$finalRedirect['destination_language']
			);

			if ($redirectUri === '') {
				return GeneralUtility::makeInstance(ErrorController::class)->pageNotFoundAction(
					$request,
					'The page with id #' . $finalRedirect['target'] . ' could not be found!',
					['code' => PageAccessFailureReasons::PAGE_NOT_FOUND]
				);
			}

			$this->logRedirect($finalRedirect, $testRequestUri, $redirectUri, $start, \microtime(TRUE), $userIpAddress);
			return new RedirectResponse($redirectUri, $finalRedirect['target_statuscode']);
		}

		return $handler->handle($request);
	}

	/**
	 * Get the redirect url from typolink if necessary
	 *
	 * @param mixed $destinationUrl
	 * @param array $requestParameters
	 * @param int $destinationLanguageUid
	 * @return string
	 * @throws UnknownLinkHandlerException
	 */
	protected function getRedirectUrl(
		$destinationUrl,
		array $requestParameters,
		int $destinationLanguageUid = 0
	): string {
		$queryParameters = [];
		if (count($requestParameters)) {
			foreach ($requestParameters as $key => $parameter) {
				if (is_array($parameter)) {
					foreach ($parameter as $subKey => $subParameter) {
						if (is_array($subParameter)) {
							foreach ($subParameter as $subSubKey => $subSubParameter) {
								$queryParameters[] = $key . '[' . $subKey . ']' . '[' . $subSubKey . ']' .
									'=' . $subSubParameter;
							}
						} else {
							$queryParameters[] = $key . '[' . $subKey . ']' . '=' . $subParameter;
						}
					}
				} else {
					$queryParameters[] = $key . '=' . $parameter;
				}
			}
		}

		// Resolve the destination URL type
		$linkService = GeneralUtility::makeInstance(LinkService::class);
		$linkDetails = $linkService->resolve($destinationUrl);
		$useTypolink = FALSE;
		if ($linkDetails['type'] === 'file') {
			/** @var File $file */
			$file = $linkDetails['file'];
			$destinationUrl = $file->getPublicUrl();
			$useTypolink = TRUE;
		} elseif ($linkDetails['type'] === 'url') {
			$destinationUrl = $linkDetails['url'] ?? '';
		} elseif ($linkDetails['type'] === 'page') {
			$destinationUrl = $linkDetails['pageuid'] ?? '';
		}

		// Get absolute links with attached query parameters
		if (\is_numeric($destinationUrl) || $useTypolink) {
			$queryParameters[] = 'L=' . $destinationLanguageUid;

			$contentObjectRenderer = GeneralUtility::makeInstance(ContentObjectRenderer::class, NULL);
			$redirectUri = $contentObjectRenderer->typoLink_URL(
				[
					'parameter' => $destinationUrl,
					'additionalParams' => '&' . \implode('&', $queryParameters),
					'forceAbsoluteUrl' => TRUE,
				]
			);
		} elseif (count($queryParameters)) {
			$delimiter = '?';
			if (str_contains($destinationUrl, '?')) {
				$delimiter = '&';
			}

			$destinationUrl .= $delimiter . \implode('&', $queryParameters);
		}

		return $redirectUri ?? $destinationUrl;
	}

	/**
	 * Logs the redirect to our own log database table
	 *
	 * @param array $routeRow
	 * @param string $requestUri
	 * @param string $redirectUri
	 * @param float $start
	 * @param float $end
	 * @param string $ipAddress
	 * @return void
	 * @throws Exception
	 * @throws AspectNotFoundException
	 */
	private function logRedirect(
		array $routeRow,
		string $requestUri,
		string $redirectUri,
		float $start,
		float $end,
		string $ipAddress
	): void {
		$connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
		$queryBuilder = $connectionPool->getQueryBuilderForTable(
			'tx_sgroutes_domain_model_log'
		);
		$queryBuilder->getRestrictions()->removeAll();
		$categories = $queryBuilder->select('c.title')
			->from('tx_sgroutes_domain_model_category', 'c')
			->innerJoin('c', 'tx_sgroutes_domain_model_redirect_category', 'mm', 'c.uid = mm.uid_foreign')
			->where(
				$queryBuilder->expr()->eq(
					'mm.uid_local',
					$queryBuilder->createNamedParameter($routeRow['uid'], \PDO::PARAM_INT)
				)
			)->orderBy('mm.sorting')->executeQuery()->fetchAllAssociative();

		$ipAnonymizer = GeneralUtility::makeInstance(IpAnonymizerService::class);
		$ipAddressForLog = $ipAnonymizer->anonymize($ipAddress);

		$insertRow = [
			'pid' => $routeRow['pid'],
			'use_regular_expression' => $routeRow['is_regexp'],
			'source_url' => $routeRow['source_path'],
			'source_host' => $routeRow['source_host'],
			'destination_url' => $routeRow['target'],
			'redirect_url_parameters' => $routeRow['respect_query_parameters'],
			'redirect_code' => $routeRow['target_statuscode'],
			'description' => $routeRow['description'] ?? '',
			'categories' => '',
			'request_url' => $requestUri,
			'redirect_url' => $redirectUri,
			'execution_duration' => 1000 * ($end - $start),
			'ip_address' => $ipAddressForLog,
			'crdate' => $GLOBALS['EXEC_TIME']
		];

		if ($categories) {
			/** @var array $category */
			foreach ($categories as $category) {
				if ($insertRow['categories'] !== '') {
					$insertRow['categories'] .= ', ';
				}
				$insertRow['categories'] .= $category['title'];
			}
		}

		$queryBuilder = $connectionPool->getQueryBuilderForTable(
			'tx_sgroutes_domain_model_log'
		);
		$queryBuilder->insert('tx_sgroutes_domain_model_log')->values($insertRow)->executeStatement();

		// @ todo: Somehow, this crashes our FE/ajax Call hard when used,,, makes us not find plugindata etc.
		$routeRepository = GeneralUtility::makeInstance(RouteRepository::class);
		$routeRepository->incrementHitCountForRoute($routeRow['uid'], $ipAddress);
	}
}
