<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgRoutes\Updates;

use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;
use TYPO3\CMS\Install\Updates\ChattyInterface;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

/**
 * Class PageNotFoundHandlingUpdate
 *
 * @package SGalinski\SgRoutes\Updates
 */
#[UpgradeWizard('tx_sgroutes_move2redirects')]
class MoveToRedirectsTableUpdate implements UpgradeWizardInterface, ChattyInterface {
	/**
	 * @var OutputInterface
	 */
	protected $output;

	/**
	 * @inheritDoc
	 */
	public function getTitle(): string {
		return 'Migrates sg_routes records from our custom table to core redirects table';
	}

	/**
	 * @inheritDoc
	 */
	public function getDescription(): string {
		return '';
	}

	/**
	 * @inheritDoc
	 */
	public function executeUpdate(): bool {
		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
			->getQueryBuilderForTable('tx_sgroutes_domain_model_route');
		$result = $queryBuilder->select('*')->from('tx_sgroutes_domain_model_route')->executeQuery();
		$siteFinder = GeneralUtility::makeInstance(SiteFinder::class);
		while ($row = $result->fetchAssociative()) {
			try {
				$siteConfiguration = $siteFinder->getSiteByPageId($row['pid']);
			} catch (SiteNotFoundException $e) {
				// Not sure how this could happen, but the page this route record is on has no site root
				continue;
			}

			$dataArray = [
				'pid' => $siteConfiguration->getRootPageId(),
				'is_regexp' => $row['use_regular_expression'],
				'hitcount' => $row['hits'],
				'lasthiton' => $row['last_hit'],
				'target_statuscode' => $row['redirect_code'],
				'source_url_case_sensitive' => $row['source_url_case_sensitive'],
				'description' => $row['description'],
				'categories' => $row['categories'],
				'destination_language' => $row['destination_language'],
				'force_https' => 1, // its 2022....
				'protected' => 1, // we should protect our ones
				'respect_query_parameters' => $row['redirect_url_parameters'],
				'keep_query_parameters' => $row['redirect_url_parameters'],
				'disable_hitcount' => 1,
				'source_host' => $siteConfiguration->getBase()->getHost(),
				'source_path' => $row['source_url'],
				'target' => $row['destination_url'],
				'crdate' => $row['crdate'],
				'tstamp' => $row['tstamp'],
			];

			// our old record used a preg_replace whenever we used is_regex. This will only work now
			// when using the correct field.
			if ($row['use_regular_expression'] === 1) {
				$dataArray['regular_expression_replace_pattern'] = $row['source_url'];
				$dataArray['target'] = '';
			}

			$sysRedirectsConnection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable(
				'sys_redirect'
			);
			$sysRedirectsConnection->insert('sys_redirect', $dataArray);
			$newRedirectUid = $sysRedirectsConnection->lastInsertId();
			$queryBuilderMM = GeneralUtility::makeInstance(ConnectionPool::class)
				->getQueryBuilderForTable('tx_sgroutes_domain_model_route_category');
			$currentMMs = $queryBuilderMM->select('uid_foreign', 'uid_local', 'sorting')
				->from('tx_sgroutes_domain_model_route_category')->where(
					$queryBuilder->expr()->eq('uid_local', $row['uid'])
				)->executeQuery()
				->fetchAllAssociative();
			// @todo: for records where we only got automatic routing, could we remove the automatic flag?
			// if so, we need to check if it has the setUsedForAutomatedRouting flag on category
			foreach ($currentMMs as $singleMM) {
				$sysRedirectsMMConnection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable(
					'tx_sgroutes_domain_model_redirect_category'
				);
				$sysRedirectsMMConnection->insert('tx_sgroutes_domain_model_redirect_category', [
					'uid_foreign' => $singleMM['uid_foreign'],
					'uid_local' => $newRedirectUid,
					'sorting' => $singleMM['sorting']
				]);
			}
		}

		return TRUE;
	}

	/**
	 * @inheritDoc
	 */
	public function updateNecessary(): bool {
		$connection = GeneralUtility::makeInstance(ConnectionPool::class)
			->getConnectionForTable('tx_sgroutes_domain_model_route');
		if ($connection->getSchemaManager()->tablesExist('tx_sgroutes_domain_model_route')) {
			$queryBuilder = $connection->createQueryBuilder();
			return $queryBuilder->select('uid')->from('tx_sgroutes_domain_model_route')->executeQuery()->rowCount() > 0;
		}

		return FALSE;
	}

	/**
	 * @inheritDoc
	 */
	public function getPrerequisites(): array {
		return [];
	}

	/**
	 * @param OutputInterface $output
	 */
	public function setOutput(OutputInterface $output): void {
		$this->output = $output;
	}
}
