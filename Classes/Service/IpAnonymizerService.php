<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgRoutes\Service;

use TYPO3\CMS\Core\SingletonInterface;

/**
 * Class IpAnonymizerService
 *
 * @package SGalinski\SgRoutes\Service
 */
class IpAnonymizerService implements SingletonInterface {
	/**
	 * @var string IPv4 netmask used to anonymize IPv4 addresses.
	 */
	protected $ipv4NetMask = "255.255.255.0";

	/**
	 * @var string IPv6 netmask used to anonymize IPv6 addresses.
	 */
	protected $ipv6NetMask = "ffff:ffff:ffff:ffff:0000:0000:0000:0000";

	/**
	 * Anonymize an IPv4 or IPv6 address.
	 *
	 * @param string $address IP address that must be anonymized
	 * @return string The anonymized IP address. Returns an empty string when the IP address is invalid.
	 */
	public function anonymize(string $address): string {
		$packedAddress = inet_pton($address);

		if (strlen($packedAddress) === 4) {
			return $this->anonymizeIPv4($address);
		}

		if (strlen($packedAddress) === 16) {
			return $this->anonymizeIPv6($address);
		}

		return "";
	}

	/**
	 * Anonymize an IPv4 address
	 *
	 * @param string $address IPv4 address
	 * @return string Anonymized address
	 */
	public function anonymizeIPv4(string $address): string {
		return inet_ntop(inet_pton($address) & inet_pton($this->ipv4NetMask));
	}

	/**
	 * Anonymize an IPv6 address
	 *
	 * @param string $address address
	 * @return string Anonymized address
	 */
	public function anonymizeIPv6(string $address): string {
		return inet_ntop(inet_pton($address) & inet_pton($this->ipv6NetMask));
	}
}
