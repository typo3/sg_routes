<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgRoutes\Service;

use SGalinski\SgRoutes\Domain\Model\Category;
use SGalinski\SgRoutes\Domain\Repository\CategoryRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException;

class RouteDataService {
	/**
	 * Returns the automated routes category or creates one if none is found
	 *
	 * @param int $rootPageUid
	 * @param string $siteName
	 * @return Category|null
	 * @throws IllegalObjectTypeException
	 */
	public function getAutomatedRoutesCategory($rootPageUid, $siteName) {
		$rootPageUid = (int) $rootPageUid;
		if ($rootPageUid <= 0) {
			return NULL;
		}

		$categoryRepository = GeneralUtility::makeInstance(CategoryRepository::class);
		/** @var Category $category */
		$category = $categoryRepository->findAutomatedRoutingByRootPage($rootPageUid);
		if (!$category) {
			$category = new Category();
			$category->setTitle('Automated Routing (' . $siteName . ')');
			$category->setDescription('Routing records automatically generated based on page slug updates.');
			$category->setUsedForAutomatedRouting(TRUE);
			$category->setPid($rootPageUid);
			$categoryRepository->add($category);
			$categoryRepository->persistAll();
		}

		return $category;
	}
}
