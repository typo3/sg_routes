<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgRoutes\Service;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * Class SGalinski\SgRoutes\Service\RoutingService
 */
class RoutingService {
	/**
	 * Generate and return htaccess entries for all routes
	 *
	 * NOTE: Some features might not be supported correctly.
	 *
	 * @param array $routes
	 * @return string
	 */
	public function getHtaccessEntries(array $routes = []): string {
		$htaccessEntries = '';
		foreach ($routes as $route) {
			$route['description'] = \trim($route['description'] ?? '');
			if ($route['description']) {
				$htaccessEntries .= LF . \preg_replace('/(^)/m', '# $1', $route['description']) . LF;
			}
			$directive = 'Redirect';
			$useRegularExpression = $route['is_regexp'] ?? FALSE;
			if ($useRegularExpression) {
				$directive = 'RedirectMatch';
			}

			$destinationUrl = $route['target'] ?? '';
			$sourceUrlCaseSensitive = $route['source_url_case_sensitive'] ?? FALSE;
			$sourceUrl = $route['source_path'] ?? '';
			if (!$sourceUrlCaseSensitive) {
				$directive = 'RedirectMatch';
				$sourceUrl = '(?i)' . \addslashes($sourceUrl);
			}

			if (\is_numeric($destinationUrl) || str_starts_with($destinationUrl, 't3://')) {
				$contentObjectRenderer = GeneralUtility::makeInstance(ContentObjectRenderer::class);
				$destinationUrl = $contentObjectRenderer->typoLink(
					'',
					[
						'parameter' => $destinationUrl,
						'returnLast' => 'url'
					]
				);
			}

			if ($destinationUrl !== '') {
				// only add the htaccess rule if there is a destination, because otherwise it will be 404 anyway
				$htaccessEntries .= $directive . ' ' . $route['target_statuscode'] . ' '
					. $sourceUrl . ' ' . $destinationUrl . LF;
			}
		}

		return $htaccessEntries;
	}

	/**
	 * Returns the root uid based on the given page uid
	 *
	 * @param int $pageUid
	 * @return int
	 */
	public static function getRootUidByPageUid(int $pageUid): int {
		$out = 0;
		$pageRootline = BackendUtility::BEgetRootLine($pageUid);
		foreach ($pageRootline as $pageData) {
			if ($pageData['is_siteroot']) {
				$out = (int) $pageData['uid'];
				break;
			}
		}

		return $out;
	}
}
