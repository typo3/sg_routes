<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgRoutes\Event\Listener;

use SGalinski\SgRoutes\Domain\Repository\RouteRepository;
use TYPO3\CMS\Backend\Controller\Event\ModifyPageLayoutContentEvent;
use TYPO3\CMS\Backend\Routing\Exception\RouteNotFoundException;
use TYPO3\CMS\Backend\Routing\UriBuilder;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Routing\SiteMatcher;
use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

class ModifyPageLayoutContentEventListener {
	protected SiteMatcher $siteMatcher;

	protected RouteRepository $routeRepository;

	protected UriBuilder $uriBuilder;

	public function __construct(
		SiteMatcher $siteMatcher,
		RouteRepository $routeRepository,
		UriBuilder $uriBuilder
	) {
		$this->siteMatcher = $siteMatcher;
		$this->routeRepository = $routeRepository;
		$this->uriBuilder = $uriBuilder;
	}

	/**
	 * @throws InvalidQueryException
	 */
	public function __invoke(ModifyPageLayoutContentEvent $event): void {
		$pageId = $event->getRequest()->getQueryParams()['id'] ?? 0;
		if ($pageId === 0) {
			return;
		}

		$sourceUrls = [];
		$site = $this->siteMatcher->matchByPageId($pageId);
		$pageRow = BackendUtility::getRecord('pages', $pageId, 'slug');
		foreach ($site->getLanguages() as $language) {
			$languageId = $language->getLanguageId();
			$languageBase = $language->getBase();

			if ($languageId === 0) {
				$sourceUrls[] = rtrim($languageBase->getPath(), '/') . '/' . ltrim($pageRow['slug'], '/');
			} else {
				$translatedRowSearch = BackendUtility::getRecordLocalization('pages', $pageId, $languageId);
				if ($translatedRowSearch) {
					$translatedRow = $translatedRowSearch[0];
					$sourceUrls[] = rtrim($languageBase->getPath(), '/') . '/' . ltrim($translatedRow['slug'], '/');
				}
			}
		}

		$urlsWithRedirect = [];
		foreach ($sourceUrls as $sourceUrl) {
			if ($this->routeRepository->sourceUrlRouteExists($site->getRootPageId(), $sourceUrl)) {
				$urlsWithRedirect[] = $sourceUrl;
			}
		}

		if (\count($urlsWithRedirect) > 0) {
			$returnUrl = (string) $event->getRequest()->getUri();
			$urls = [];

			// wrap each of the conflicting URL's with a link to the sg_routes list module,
			// where the list is already filtered & shows the results for the conflicting URL
			foreach ($urlsWithRedirect as $redirectUrl) {
				$params = [
					'tx_sgroutes_web_sgroutesroute' => [
						'filters' => [
							'search' => $redirectUrl
						]
					],
					'returnUrl' => $returnUrl,
					'id' => $pageId
				];

				try {
					$uri = (string) $this->uriBuilder->buildUriFromRoute('web_SgRoutesRoute', $params);
					$redirectUrl = '<strong><a href="' . $uri . '">' . $redirectUrl . '</a></strong>';
				} catch (RouteNotFoundException $e) {
					$redirectUrl = '<strong>' . $redirectUrl . '</strong>';
				}

				$urls[] = $redirectUrl;
			}

			$urlsString = implode(', ', $urls);
			$message = LocalizationUtility::translate('backend.pageSlugConflict', 'SgRoutes', [$urlsString]);
			$event->addHeaderContent('<div class="alert alert-danger" role="alert">' . $message . '</div>');
		}
	}
}
