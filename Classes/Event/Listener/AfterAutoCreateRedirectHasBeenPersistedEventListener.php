<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgRoutes\Event\Listener;

use SGalinski\SgRoutes\Domain\Model\Route;
use SGalinski\SgRoutes\Domain\Repository\RouteRepository;
use SGalinski\SgRoutes\Service\RouteDataService;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\LinkHandling\Exception\UnknownLinkHandlerException;
use TYPO3\CMS\Core\LinkHandling\LinkService;
use TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException;
use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;
use TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException;
use TYPO3\CMS\Redirects\Event\AfterAutoCreateRedirectHasBeenPersistedEvent;

class AfterAutoCreateRedirectHasBeenPersistedEventListener {
	protected RouteRepository $routeRepository;

	protected RouteDataService $routeDataService;

	protected LinkService $linkService;

	protected Context $context;

	protected ConnectionPool $connectionPool;

	protected ExtensionConfiguration $extensionConfiguration;

	public function __construct(
		RouteRepository $routeRepository,
		RouteDataService $routeDataService,
		LinkService $linkService,
		Context $context,
		ConnectionPool $connectionPool,
		ExtensionConfiguration $extensionConfiguration
	) {
		$this->routeRepository = $routeRepository;
		$this->routeDataService = $routeDataService;
		$this->linkService = $linkService;
		$this->context = $context;
		$this->connectionPool = $connectionPool;
		$this->extensionConfiguration = $extensionConfiguration;
	}

	/**
	 * @param AfterAutoCreateRedirectHasBeenPersistedEvent $event
	 * @throws IllegalObjectTypeException
	 * @throws InvalidQueryException
	 * @throws UnknownLinkHandlerException
	 * @throws UnknownObjectException
	 */
	public function __invoke(AfterAutoCreateRedirectHasBeenPersistedEvent $event): void {
		$site = $event->getSlugRedirectChangeItem()->getSite();
		$rootPageId = $site->getRootPageId();
		$automatedRoutingCategory = $this->routeDataService->getAutomatedRoutesCategory(
			$rootPageId,
			$site->getBase()
		);

		if (!$automatedRoutingCategory) {
			return;
		}

		$targetLink = $this->linkService->asString([
			'type' => 'page',
			'pageuid' => $event->getSlugRedirectChangeItem()->getPageId(),
			'parameters' => '_language=' . $event->getSlugRedirectChangeItem()->getSiteLanguage()->getLanguageId(),
		]);
		// See if there are any automatically created routes,
		// with the new destination URL as the source_url. Delete those routes, to prevent conflicts.
		// Basically avoid the conflict warning, rendered in PageLayoutController->addRoutesWarningMessage()
		$conflictingRoutes = $this->routeRepository->findByCategoryAndSource(
			$rootPageId,
			$automatedRoutingCategory->getUid(),
			$targetLink
		);

		if (\count($conflictingRoutes)) {
			/** @var Route $conflictingRoute */
			foreach ($conflictingRoutes as $conflictingRoute) {
				$this->routeRepository->remove($conflictingRoute);
			}

			$this->routeRepository->persistAll();
		}

		// update all automatic redirects with the old destination URL to the new destination URL
		// if the previous source url is the same, we delete them (also prevents possible duplicates
		// otherwise we could also return here)
		$originalSlug = $event->getSlugRedirectChangeItem()->getOriginal()['slug'];
		$existingSlugRoutes = $this->routeRepository->findByCategoryAndDestination(
			$rootPageId,
			$automatedRoutingCategory->getUid(),
			$originalSlug
		);
		if (\count($existingSlugRoutes) > 0) {
			/** @var Route $route */
			foreach ($existingSlugRoutes as $route) {
				if ($route->getSourceUrl() === $targetLink) {
					$this->routeRepository->remove($route);
				} else {
					$route->setDestinationUrl($targetLink);
					$route->setTstamp($GLOBALS['EXEC_TIME']);
					$this->routeRepository->update($route);
				}
			}

			$this->routeRepository->persistAll();
		}

		// don't create redirects for short slugs (this causes harm in most cases)
		// NOTE: We don't need to check slugs from existing pages, because TYPO3 has a duplicate
		// check already integrated.
		if (strlen($originalSlug) < 3) {
			return;
		}

		// don't create a redirect if the old source URL was added manually already in any category
		$routeExists = $this->routeRepository->sourceUrlRouteExists($rootPageId, $originalSlug);
		if ($routeExists) {
			return;
		}

		try {
			$respectQueryParametersByDefaultForAutomaticRedirects = (string) $this->extensionConfiguration
				->get('sg_routes', 'respectQueryParametersByDefaultForAutomaticRedirects');
		} catch (\Exception $exception) {
			$respectQueryParametersByDefaultForAutomaticRedirects = 0;
		}

		try {
			$keepQueryParametersByDefaultForAutomaticRedirects = (string) $this->extensionConfiguration
				->get('sg_routes', 'keepQueryParametersByDefaultForAutomaticRedirects');
		} catch (\Exception $exception) {
			$keepQueryParametersByDefaultForAutomaticRedirects = 0;
		}

		$qb = $this->connectionPool->getQueryBuilderForTable('sys_redirect');
		$qb->update('sys_redirect')
			->set('respect_query_parameters', $respectQueryParametersByDefaultForAutomaticRedirects)
			->set('keep_query_parameters', $keepQueryParametersByDefaultForAutomaticRedirects)
			->where(
				$qb->expr()->eq(
					'uid',
					$qb->createNamedParameter($event->getRedirectRecord()['uid'], Connection::PARAM_INT)
				)
			)->executeStatement();
		$sysRedirectsMMConnection = $this->connectionPool->getConnectionForTable(
			'tx_sgroutes_domain_model_redirect_category'
		);
		$sysRedirectsMMConnection->insert('tx_sgroutes_domain_model_redirect_category', [
			'uid_foreign' => $automatedRoutingCategory->getUid(),
			'uid_local' => $event->getRedirectRecord()['uid'],
			'sorting' => 0
		]);
	}
}
