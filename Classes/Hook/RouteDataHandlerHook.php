<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgRoutes\Hook;

use SGalinski\SgRoutes\Domain\Repository\RouteRepository;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
use TYPO3\CMS\Core\Type\ContextualFeedbackSeverity;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

class RouteDataHandlerHook {
	/**
	 * Called after a record was edited or added.
	 *
	 * @param string $status DataHandler operation status, either 'new' or 'update'
	 * @param string $table The DB table the operation was carried out on
	 * @param mixed $id The record's uid for update records, a string to look the record's uid up after it has been created
	 * @param array $fieldArray Array of changed fields and their new values
	 * @throws Exception
	 */
	public function processDatamap_postProcessFieldArray(string $status, string $table, $id, array &$fieldArray) {
		if ($table !== 'sys_redirect') {
			return;
		}

		$id = (int) $id;
		$sourceUrl = '';
		$sourceHost = '*';
		if (isset($fieldArray['source_path'])) {
			$sourceUrl = \trim($fieldArray['source_path']);
		}

		if (isset($fieldArray['source_host'])) {
			$sourceHost = \trim($fieldArray['source_host']);
		}

		if ($status === 'new' || isset($fieldArray['pid'])) {
			$pid = (int) $fieldArray['pid'];
		} else {
			$oldValues = BackendUtility::getRecord($table, $id, 'pid, source_path');
			$pid = (int) $oldValues['pid'];

			if ($sourceUrl === '' && isset($oldValues['source_path'])) {
				$sourceUrl = \trim($oldValues['source_path']);
			}

			if ($sourceHost === '' && isset($oldValues['source_host'])) {
				$sourceHost = \trim($oldValues['source_host']);
			}
		}

		$routeRepository = GeneralUtility::makeInstance(RouteRepository::class);
		$routeExistsForSourceUrl = $routeRepository->sourceUrlRouteExists($pid, $sourceUrl, [$id], $sourceHost);
		if ($routeExistsForSourceUrl) {
			/** @var FlashMessage $message */
			$message = GeneralUtility::makeInstance(
				FlashMessage::class,
				LocalizationUtility::translate(
					'LLL:EXT:sg_routes/Resources/Private/Language/locallang_db.xlf:error.invalidSourceUrl',
					NULL,
					[$sourceUrl]
				),
				'', // header is optional
				ContextualFeedbackSeverity::ERROR,
				TRUE // whether message should be stored in session
			);
			/** @var FlashMessageService $flashMessageService */
			$flashMessageService = GeneralUtility::makeInstance(FlashMessageService::class);
			$flashMessageService->getMessageQueueByIdentifier()->enqueue($message);
			unset($fieldArray['source_path']);
		}
	}
}
