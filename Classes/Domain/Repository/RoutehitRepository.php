<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgRoutes\Domain\Repository;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\QuerySettingsInterface;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * Class RoutehitRepository
 *
 * @package SGalinski\SgRoutes\Domain\Repository
 */
class RoutehitRepository extends Repository {
	/**
	 * The table name for this repository
	 */
	public const TABLE_NAME = 'tx_sgroutes_domain_model_routehit';

	/**
	 * Ordered descending by crdate
	 *
	 * @var array
	 */
	protected $defaultOrderings = [
		'crdate' => QueryInterface::ORDER_DESCENDING
	];

	/**
	 * We explicitly ignore the storage page here, because every route hit record automatically gets the corresponding route's pid set
	 */
	public function initializeObject(): void {
		/** @var QuerySettingsInterface $defaultQuerySettings */
		$defaultQuerySettings = GeneralUtility::makeInstance(QuerySettingsInterface::class);
		$defaultQuerySettings->setRespectStoragePage(FALSE);
		$this->setDefaultQuerySettings($defaultQuerySettings);
	}

	/**
	 * Returns the latest route hit for the given $routeUid & $ipAddress
	 *
	 * @param int $routeUid
	 * @param string $ipAddress
	 * @return object|NULL
	 */
	public function findLatestRoutehit(int $routeUid, string $ipAddress): ?object {
		$query = $this->createQuery();
		$constraints = [];
		$constraints[] = $query->equals('ip_address', $ipAddress);
		$constraints[] = $query->equals('route_uid', $routeUid);

		$query->matching($query->logicalAnd(...$constraints))->setLimit(1);

		return $query->execute()->getFirst();
	}
}
