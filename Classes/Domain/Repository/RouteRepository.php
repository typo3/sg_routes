<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgRoutes\Domain\Repository;

use DateMalformedStringException;
use InvalidArgumentException;
use SGalinski\SgRoutes\Domain\Model\Route;
use SGalinski\SgRoutes\Domain\Model\Routehit;
use SGalinski\SgRoutes\Service\IpAnonymizerService;
use TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException;
use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;
use TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * Route Repository
 */
class RouteRepository extends Repository {
	/**
	 * The amount of time that needs to pass, between now and the last route hit for a given IP address, before
	 * a new hit for the same ip address is counted.
	 */
	public const HIT_COUNT_SPAM_PROTECTION_TIME = "-30 minutes";

	/**
	 * @var RoutehitRepository
	 */
	protected $routehitRepository;

	/**
	 * @var IpAnonymizerService
	 */
	protected $ipAnonymizerService;

	/**
	 * @param RoutehitRepository $routehitRepository
	 */
	public function injectRouteHitRepository(RoutehitRepository $routehitRepository): void {
		$this->routehitRepository = $routehitRepository;
	}

	/**
	 * @param IpAnonymizerService $ipAnonymizerService
	 */
	public function injectIpAnonymizerService(IpAnonymizerService $ipAnonymizerService): void {
		$this->ipAnonymizerService = $ipAnonymizerService;
	}

	/**
	 * initializes the object
	 */
	public function initializeObject(): void {
		$querySettings = $this->createQuery()->getQuerySettings();
		$querySettings->setIgnoreEnableFields(TRUE);
		$querySettings->setEnableFieldsToBeIgnored(['disabled']);
		$this->setDefaultQuerySettings($querySettings);
	}

	/**
	 * Queries the redirect records based on filters
	 *
	 * @param int $pageUid
	 * @param array $filters
	 * @param boolean $raw
	 * @return mixed
	 * @throws InvalidQueryException
	 * @throws InvalidArgumentException
	 */
	public function findRoutes(int $pageUid, array $filters = [], $raw = FALSE) {
		$query = $this->createQuery();

		$querySettings = $query->getQuerySettings();
		$querySettings->setStoragePageIds([$pageUid]);
		$query->setQuerySettings($querySettings);

		$query->setOrderings(
			[
				'priority' => QueryInterface::ORDER_ASCENDING
			]
		);

		$constraints = [];
		if (isset($filters['categories']) && \is_array($filters['categories']) && \count($filters['categories'])) {
			$categoryConstraints = [];
			foreach ($filters['categories'] as $category) {
				if ((int) $category) {
					$categoryConstraints[] = $query->contains('categories', $category);
				}
			}

			if (\count($categoryConstraints)) {
				$constraints[] = $query->logicalOr(...$categoryConstraints);
			}
		}

		if (isset($filters['search']) && $filters['search'] !== '') {
			$searchConstraints = [];
			$searchConstraints[] = $query->equals('uid', $filters['search']);
			$searchConstraints[] = $query->like('source_path', '%' . $filters['search'] . '%');
			$searchConstraints[] = $query->like('target', '%' . $filters['search'] . '%');
			$searchConstraints[] = $query->like('target_statuscode', '%' . $filters['search'] . '%');
			$searchConstraints[] = $query->like('description', '%' . $filters['search'] . '%');
			$searchConstraints[] = $query->like('source_host', '%' . $filters['search'] . '%');
			$constraints[] = $query->logicalOr(...$searchConstraints);
		}

		if (isset($filters['filterFromDate']) && $filters['filterFromDate'] !== '') {
			try {
				$fromTime = new \DateTime($filters['filterFromDate']);
				if ($fromTime->getTimestamp() > 0) {
					$constraints[] = $query->greaterThanOrEqual('lasthiton', $fromTime);
				}
			} catch (DateMalformedStringException $e) {
				// do nothing
			}
		}

		if (isset($filters['filterToDate']) && $filters['filterToDate'] !== '') {
			try {
				$toTime = new \DateTime($filters['filterToDate']);
				if ($toTime->getTimestamp() > 0) {
					$constraints[] = $query->lessThanOrEqual('lasthiton', $toTime);
				}
			} catch (DateMalformedStringException $e) {
				// do nothing
			}
		}

		if (isset($filters['hitCount']) && $filters['hitCount'] !== '') {
			$hitCount = (int) $filters['hitCount'];
			if ($hitCount === 0) {
				$constraints[] = $query->equals('hitcount', 0);
			} else {
				$constraints[] = $query->greaterThanOrEqual('hitcount', $hitCount);
			}
		}

		if (\count($constraints) > 1) {
			$query->matching($query->logicalAnd(...$constraints));
		} elseif (\count($constraints)) {
			$query->matching($constraints[0]);
		}

		return $query->execute($raw);
	}

	/**
	 * Queries the redirect records based on category and destination URL
	 *
	 * @param int $pageUid
	 * @param int $categoryUid
	 * @param string $destinationUrl
	 * @return array|QueryResultInterface
	 * @throws InvalidQueryException
	 */
	public function findByCategoryAndDestination(int $pageUid, int $categoryUid, string $destinationUrl) {
		$query = $this->createQuery();
		$querySettings = $query->getQuerySettings();
		$querySettings->setStoragePageIds([$pageUid]);
		$query->setQuerySettings($querySettings);
		$query->matching(
			$query->logicalAnd(
				$query->contains('categories', (int) $categoryUid),
				$query->equals('target', \trim($destinationUrl))
			)
		);

		return $query->execute();
	}

	/**
	 * Queries the redirect records based on category and destination URL
	 *
	 * @param int $pageUid
	 * @param int $categoryUid
	 * @param string $sourceUrl
	 * @return array|QueryResultInterface
	 * @throws InvalidQueryException
	 */
	public function findByCategoryAndSource($pageUid, $categoryUid, $sourceUrl) {
		$query = $this->createQuery();
		$querySettings = $query->getQuerySettings();
		$querySettings->setStoragePageIds([$pageUid]);
		$query->setQuerySettings($querySettings);
		$query->matching(
			$query->logicalAnd(
				$query->contains('categories', (int) $categoryUid),
				$query->equals('source_path', \trim($sourceUrl))
			)
		);

		return $query->execute();
	}

	/**
	 * Queries the redirect records based on category and destination URL
	 *
	 * @param int $pageUid
	 * @param string $sourceUrl
	 * @param array $excludeUids
	 * @param string $sourceHost
	 * @return bool
	 * @throws InvalidQueryException
	 */
	public function sourceUrlRouteExists(
		int $pageUid,
		string $sourceUrl,
		array $excludeUids = [],
		string $sourceHost = '*'
	): bool {
		$query = $this->createQuery();
		$querySettings = $query->getQuerySettings();
		$querySettings->setStoragePageIds([$pageUid]);
		$query->setQuerySettings($querySettings);

		$constraints = [
			$query->equals('source_path', \trim($sourceUrl)),
			$query->equals('source_host', \trim($sourceHost))
		];

		if (\count($excludeUids)) {
			$constraints[] = $query->logicalNot($query->in('uid', $excludeUids));
		}

		$query->matching($query->logicalAnd(...$constraints));
		$query->setLimit(1);

		return \count($query->execute(TRUE)) > 0;
	}

	/**
	 * Commits the object updates to the database
	 */
	public function persistAll(): void {
		$this->persistenceManager->persistAll();
	}

	/**
	 * Increments the "hits" for a route by one
	 *
	 * @param int $routeUid
	 * @param string $ipAddress
	 */
	public function incrementHitCountForRoute(int $routeUid, string $ipAddress): void {
		/** @var Route|NULL $route */
		$route = $this->findByUid($routeUid);
		if (!$route) {
			return;
		}

		$countThisRoutehit = FALSE;
		$ipAddress = $this->ipAnonymizerService->anonymize($ipAddress);
		/** @var Routehit|NULL $latestRoutehit */
		$latestRoutehit = $this->routehitRepository->findLatestRoutehit($routeUid, $ipAddress);
		// we count this route hit only,
		// 	* if we cannot find an existing one for the given $ipAddress
		// OR
		// 	* if the latest route hit found, is older than self::HIT_COUNT_SPAM_PROTECTION_TIME

		if ($latestRoutehit === NULL) {
			$countThisRoutehit = TRUE;
		} else {
			$latestRoutehitCrdate = $latestRoutehit->getCrdate();
			if ($latestRoutehitCrdate < strtotime(self::HIT_COUNT_SPAM_PROTECTION_TIME)) {
				$countThisRoutehit = TRUE;
			}
		}

		$newRouteHit = new Routehit();
		$newRouteHit->setPid($route->getPid());
		$newRouteHit->setIpAddress($ipAddress);
		$newRouteHit->setRouteUid($routeUid);
		$newRouteHit->setCrdate(time());

		try {
			$this->routehitRepository->add($newRouteHit);
			// explicitly calling the route hit persistenceManager here, otherwise the changes won't be persisted
			$this->routehitRepository->persistenceManager->persistAll();
		} catch (IllegalObjectTypeException $e) {
			// not used -> redirect should still work, but the hit won't count
		}

		if ($countThisRoutehit) {
			$route->setLastHit(time());
			$route->setHits($route->getHits() + 1);
			try {
				$this->update($route);
				// calling persistAll() here manually is needed, otherwise the changes won't be persisted, since
				// this method is executed from within a middleware & performs a redirect
				$this->persistAll();
			} catch (IllegalObjectTypeException|UnknownObjectException $e) {
				// not used -> redirect should still work, but the hit won't count
			}
		}
	}

	/**
	 * Resets the "hits" for a route to 0
	 *
	 * @param int $routeUid
	 * @throws IllegalObjectTypeException
	 * @throws UnknownObjectException
	 */
	public function resetHitCountForRoute(int $routeUid): void {
		/** @var Route|NULL $route */
		$route = $this->findByUid($routeUid);
		if (!$route || $route->getHits() === 0) {
			return;
		}

		$route->setLastHit(0);
		$route->setHits(0);
		$this->update($route);
	}
}
