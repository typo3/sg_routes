<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgRoutes\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * The Route model
 */
class Route extends AbstractEntity {
	/**
	 * @var boolean
	 */
	protected $useRegularExpression = FALSE;

	/**
	 * @var string
	 */
	protected $sourceUrl = '';

	/**
	 * @var string
	 */
	protected $sourceHost = '';

	/**
	 * @var string
	 */
	protected $destinationUrl = '';

	/**
	 * @var int
	 */
	protected $destinationLanguage = 0;

	/**
	 * @var boolean
	 */
	protected $redirectUrlParameters = FALSE;

	/**
	 * @var string
	 */
	protected $redirectCode = '301';

	/**
	 * @var string
	 */
	protected $description = '';

	/**
	 * categories
	 *
	 * @var ObjectStorage<Category>
	 */
	protected $categories;

	/**
	 * @var int
	 */
	protected $crdate = 0;

	/**
	 * @var int
	 */
	protected $tstamp = 0;

	/**
	 * @var int
	 */
	protected $createdon = 0;

	/**
	 * @var int
	 */
	protected $updatedon = 0;

	/**
	 * The amount of times a route was hit (this value is set in the RedirectResolver middleware)
	 *
	 * @var int
	 */
	protected $hits = 0;

	/**
	 * @var int
	 */
	protected $lastHit = 0;

	/**
	 * @var int
	 */
	protected $priority = 10;

	/**
	 * Route object constructor
	 */
	public function __construct() {
		$this->categories = new ObjectStorage();
	}

	/**
	 * @return int
	 */
	public function getHits(): int {
		return $this->hits;
	}

	/**
	 * @param int $hits
	 */
	public function setHits(int $hits): void {
		$this->hits = $hits;
	}

	/**
	 * @return int
	 */
	public function getLastHit(): int {
		return $this->lastHit;
	}

	/**
	 * @param int $lastHit
	 */
	public function setLastHit(int $lastHit): void {
		$this->lastHit = $lastHit;
	}

	/**
	 * @return boolean
	 */
	public function getUseRegularExpression(): bool {
		return $this->useRegularExpression;
	}

	/**
	 * @param boolean $useRegularExpression
	 */
	public function setUseRegularExpression(bool $useRegularExpression): void {
		$this->useRegularExpression = $useRegularExpression;
	}

	/**
	 * @return string
	 */
	public function getSourceUrl(): string {
		return $this->sourceUrl;
	}

	/**
	 * @param string $sourceUrl
	 */
	public function setSourceUrl(string $sourceUrl): void {
		$this->sourceUrl = $sourceUrl;
	}

	/**
	 * @return string
	 */
	public function getSourceHost(): string {
		return $this->sourceHost;
	}

	/**
	 * @param string $sourceHost
	 */
	public function setSourceHost(string $sourceHost): void {
		$this->sourceHost = $sourceHost;
	}

	/**
	 * @return string
	 */
	public function getDestinationUrl(): string {
		return $this->destinationUrl;
	}

	/**
	 * @param string $destinationUrl
	 */
	public function setDestinationUrl(string $destinationUrl): void {
		$this->destinationUrl = $destinationUrl;
	}

	/**
	 * @return int
	 */
	public function getDestinationLanguage(): int {
		return $this->destinationLanguage;
	}

	/**
	 * @param int $destinationLanguage
	 */
	public function setDestinationLanguage(int $destinationLanguage): void {
		$this->destinationLanguage = $destinationLanguage;
	}

	/**
	 * @return boolean
	 */
	public function getRedirectUrlParameters(): bool {
		return $this->redirectUrlParameters;
	}

	/**
	 * @param boolean $redirectUrlParameters
	 */
	public function setRedirectUrlParameters(bool $redirectUrlParameters): void {
		$this->redirectUrlParameters = $redirectUrlParameters;
	}

	/**
	 * @return string
	 */
	public function getRedirectCode(): string {
		return $this->redirectCode;
	}

	/**
	 * @param string $redirectCode
	 */
	public function setRedirectCode(string $redirectCode): void {
		$this->redirectCode = $redirectCode;
	}

	/**
	 * @return int
	 */
	public function getPriority(): int {
		return $this->priority;
	}

	/**
	 * @param int $priority
	 */
	public function setPriority(int $priority): void {
		$this->priority = $priority;
	}

	/**
	 * @return string
	 */
	public function getDescription(): string {
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription(string $description): void {
		$this->description = $description;
	}

	/**
	 * Adds a Category
	 *
	 * @param Category $category
	 * @return void
	 */
	public function addCategory(Category $category): void {
		$this->categories->attach($category);
	}

	/**
	 * Removes a Category
	 *
	 * @param Category $category
	 * @return void
	 */
	public function removeCategory(Category $category): void {
		$this->categories->detach($category);
	}

	/**
	 * Returns the categories
	 *
	 * @return ObjectStorage $categories
	 */
	public function getCategories(): ObjectStorage {
		return $this->categories;
	}

	/**
	 * Sets the categories
	 *
	 * @param ObjectStorage $categories
	 * @return void
	 */
	public function setCategories(ObjectStorage $categories): void {
		$this->categories = $categories;
	}

	/**
	 * @return int
	 */
	public function getCrdate(): int {
		return $this->crdate;
	}

	/**
	 * @param int $crdate
	 */
	public function setCrdate(int $crdate): void {
		$this->crdate = $crdate;
	}

	/**
	 * @return int
	 */
	public function getTstamp(): int {
		return $this->tstamp;
	}

	/**
	 * @param int $tstamp
	 */
	public function setTstamp(int $tstamp): void {
		$this->tstamp = $tstamp;
	}

	/**
	 * @return int
	 */
	public function getCreatedon(): int {
		return $this->createdon;
	}

	/**
	 * @param int $createdon
	 */
	public function setCreatedon(int $createdon): void {
		$this->createdon = $createdon;
	}

	/**
	 * @return int
	 */
	public function getUpdatedon(): int {
		return $this->createdon;
	}

	/**
	 * @param int $updatedon
	 */
	public function setUpdatedon(int $updatedon): void {
		$this->updatedon = $updatedon;
	}
}
