<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgRoutes\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * The Route model
 */
class Log extends AbstractEntity {
	/**
	 * @var boolean
	 */
	protected $useRegularExpression = FALSE;

	/**
	 * @var string
	 */
	protected $sourceUrl = '';

	/**
	 * @var string
	 */
	protected $sourceHost = '';

	/**
	 * @var string
	 */
	protected $ipAddress = '';

	/**
	 * @var string
	 */
	protected $destinationUrl = '';

	/**
	 * @var boolean
	 */
	protected $redirectUrlParameters = FALSE;

	/**
	 * @var string
	 */
	protected $redirectCode = '';

	/**
	 * @var string
	 */
	protected $description = '';

	/**
	 * categories
	 *
	 * @var string
	 */
	protected $categories = '';

	/**
	 * @var string
	 */
	protected $requestUrl = '';

	/**
	 * @var string
	 */
	protected $redirectUrl = '';

	/**
	 * @var int
	 */
	protected $executionDuration = 0;

	/**
	 * @var int
	 */
	protected $crdate = 0;

	/**
	 * @return boolean
	 */
	public function getUseRegularExpression() {
		return $this->useRegularExpression;
	}

	/**
	 * @param boolean $useRegularExpression
	 */
	public function setUseRegularExpression($useRegularExpression) {
		$this->useRegularExpression = $useRegularExpression;
	}

	/**
	 * @return string
	 */
	public function getSourceUrl() {
		return $this->sourceUrl;
	}

	/**
	 * @param string $sourceUrl
	 */
	public function setSourceUrl($sourceUrl) {
		$this->sourceUrl = $sourceUrl;
	}

	/**
	 * @return string
	 */
	public function getSourceHost() {
		return $this->sourceHost;
	}

	/**
	 * @param string $sourceHost
	 */
	public function setSourceHost($sourceHost) {
		$this->sourceHost = $sourceHost;
	}

	/**
	 * @return string
	 */
	public function getIpAddress() {
		return $this->ipAddress;
	}

	/**
	 * @param string $ipAddress
	 */
	public function setIpAddress($ipAddress) {
		$this->ipAddress = $ipAddress;
	}

	/**
	 * @return string
	 */
	public function getDestinationUrl() {
		return $this->destinationUrl;
	}

	/**
	 * @param string $destinationUrl
	 */
	public function setDestinationUrl($destinationUrl) {
		$this->destinationUrl = $destinationUrl;
	}

	/**
	 * @return boolean
	 */
	public function getRedirectUrlParameters() {
		return $this->redirectUrlParameters;
	}

	/**
	 * @param boolean $redirectUrlParameters
	 */
	public function setRedirectUrlParameters($redirectUrlParameters) {
		$this->redirectUrlParameters = $redirectUrlParameters;
	}

	/**
	 * @return string
	 */
	public function getRedirectCode() {
		return $this->redirectCode;
	}

	/**
	 * @param string $redirectCode
	 */
	public function setRedirectCode($redirectCode) {
		$this->redirectCode = $redirectCode;
	}

	/**
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * Returns the categories
	 *
	 * @return string $categories
	 */
	public function getCategories() {
		return $this->categories;
	}

	/**
	 * Sets the categories
	 *
	 * @param string $categories
	 */
	public function setCategories($categories) {
		$this->categories = $categories;
	}

	/**
	 * @return string
	 */
	public function getRequestUrl() {
		return $this->requestUrl;
	}

	/**
	 * @param string $requestUrl
	 */
	public function setRequestUrl($requestUrl) {
		$this->requestUrl = $requestUrl;
	}

	/**
	 * @return string
	 */
	public function getRedirectUrl() {
		return $this->redirectUrl;
	}

	/**
	 * @param string $redirectUrl
	 */
	public function setRedirectUrl($redirectUrl) {
		$this->redirectUrl = $redirectUrl;
	}

	/**
	 * @return int
	 */
	public function getExecutionDuration() {
		return $this->executionDuration;
	}

	/**
	 * @param int $executionDuration
	 */
	public function setExecutionDuration($executionDuration) {
		$this->executionDuration = $executionDuration;
	}

	/**
	 * @return int
	 */
	public function getCrdate() {
		return $this->crdate;
	}

	/**
	 * @param int $crdate
	 */
	public function setCrdate($crdate) {
		$this->crdate = $crdate;
	}
}
