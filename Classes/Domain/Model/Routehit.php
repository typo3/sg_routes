<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgRoutes\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * Class RouteHit
 *
 * @package SGalinski\SgRoutes\Domain\Model
 */
class Routehit extends AbstractEntity {
	/**
	 * @var int
	 */
	protected $routeUid = 0;
	/**
	 * @var int
	 */
	protected $crdate = 0;

	/**
	 * @var string
	 */
	protected $ipAddress = '';

	/**
	 * @return int
	 */
	public function getRouteUid(): int {
		return $this->routeUid;
	}

	/**
	 * @param int $routeUid
	 */
	public function setRouteUid(int $routeUid): void {
		$this->routeUid = $routeUid;
	}

	/**
	 * @return int
	 */
	public function getCrdate(): int {
		return $this->crdate;
	}

	/**
	 * @param int $crdate
	 */
	public function setCrdate(int $crdate): void {
		$this->crdate = $crdate;
	}

	/**
	 * @return string
	 */
	public function getIpAddress(): string {
		return $this->ipAddress;
	}

	/**
	 * @param string $ipAddress
	 */
	public function setIpAddress(string $ipAddress): void {
		$this->ipAddress = $ipAddress;
	}
}
