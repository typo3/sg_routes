<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgRoutes\Error;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Error\Http\PageNotFoundException;
use TYPO3\CMS\Core\Error\PageErrorHandler\PageErrorHandlerInterface;
use TYPO3\CMS\Core\Http\RedirectResponse;
use TYPO3\CMS\Core\LinkHandling\Exception\UnknownLinkHandlerException;
use TYPO3\CMS\Core\LinkHandling\LinkService;
use TYPO3\CMS\Core\Routing\SiteRouteResult;
use TYPO3\CMS\Core\Site\Entity\Site;
use TYPO3\CMS\Core\Site\Entity\SiteLanguage;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class ParentPageErrorHandler
 *
 * @package SGalinski\SgRoutes\Error
 */
class ParentPageErrorHandler implements PageErrorHandlerInterface {
	/**
	 * @var int
	 */
	protected $errorCode = 404;

	/**
	 * @var array
	 */
	protected $configuration = [];

	/**
	 * ParentPageErrorHandler constructor.
	 *
	 * @param int $errorCode
	 * @param array $configuration
	 */
	public function __construct(int $errorCode, array $configuration) {
		$this->errorCode = $errorCode;
		$this->configuration = $configuration;
	}

	/**
	 * @param ServerRequestInterface $request
	 * @param string $message
	 * @param array $reasons
	 * @return ResponseInterface
	 * @throws PageNotFoundException
	 * @throws UnknownLinkHandlerException
	 */
	public function handlePageError(
		ServerRequestInterface $request,
		string $message,
		array $reasons = []
	): ResponseInterface {
		/** @var Site $site */
		$site = $request->getAttribute('site', NULL);
		if ($site === NULL) {
			// Not sure if this could ever happen, but we can not process this from here on, simply throw an exception
			throw new PageNotFoundException($message, 1518472189);
		}

		/** @var SiteLanguage $requestLanguage */
		$requestLanguage = $request->getAttribute('language', NULL);
		// Try to get the current request language from the site that was found above
		if ($requestLanguage instanceof SiteLanguage && $requestLanguage->isEnabled()) {
			try {
				$language = $site->getLanguageById($requestLanguage->getLanguageId());
			} catch (\InvalidArgumentException $e) {
				$language = $site->getDefaultLanguage();
			}
		} else {
			$language = $site->getDefaultLanguage();
		}

		/** @var SiteRouteResult $routeResult */
		$routeResult = $request->getAttribute('routing');
		if ($routeResult instanceof SiteRouteResult) {
			$tail = $routeResult->getTail();
			if ($tail !== 'index.php' && $tail !== '') {
				if (strpos($tail, '/', -1) !== FALSE) {
					$tail = substr($tail, 0, -1);
				}

				$tailParts = explode('/', $tail);
				array_pop($tailParts);
				$uri = $routeResult->getUri()->withPath('/' . implode('/', $tailParts));
				return new RedirectResponse($uri, $this->errorCode);
			}
		}

		if ($this->configuration['defaultPage']) {
			// No page to redirect to found, redirect to default 404 page
			$linkService = GeneralUtility::makeInstance(LinkService::class);
			$urlParams = $linkService->resolve($this->configuration['defaultPage']);
			if ($urlParams['type'] !== 'page' && $urlParams['type'] !== 'url') {
				throw new \InvalidArgumentException(
					'ParentPageErrorHandler can only handle TYPO3 urls of types "page" or "url"',
					1522826609
				);
			}

			if ($urlParams['type'] === 'url') {
				$uri = $urlParams['url'];
			} else {
				$uri = $site->getRouter()->generateUri(
					$urlParams['pageuid'],
					['_language' => $language]
				);
			}

			return new RedirectResponse($uri, $this->errorCode);
		}

		// There is no default error page defined, throw exception
		throw new PageNotFoundException($message, 1518472189);
	}
}
