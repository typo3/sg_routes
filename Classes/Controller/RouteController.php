<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgRoutes\Controller;

use Doctrine\DBAL\Exception;
use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;
use SGalinski\SgAccount\Service\BackendService;
use SGalinski\SgRoutes\Domain\Model\Category;
use SGalinski\SgRoutes\Domain\Model\Route;
use SGalinski\SgRoutes\Domain\Repository\CategoryRepository;
use SGalinski\SgRoutes\Domain\Repository\LogRepository;
use SGalinski\SgRoutes\Domain\Repository\RoutehitRepository;
use SGalinski\SgRoutes\Domain\Repository\RouteRepository;
use SGalinski\SgRoutes\Domain\Repository\SchedulerTaskRepository;
use SGalinski\SgRoutes\Pagination\Pagination;
use SGalinski\SgRoutes\Service\LicenceCheckService;
use SGalinski\SgRoutes\Service\RoutingService;
use TYPO3\CMS\Backend\Clipboard\Clipboard;
use TYPO3\CMS\Backend\Template\Components\ButtonBar;
use TYPO3\CMS\Backend\Template\Components\DocHeaderComponent;
use TYPO3\CMS\Backend\Template\ModuleTemplate;
use TYPO3\CMS\Backend\Template\ModuleTemplateFactory;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationExtensionNotConfiguredException;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationPathDoesNotExistException;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Database\Query\Restriction\WorkspaceRestriction;
use TYPO3\CMS\Core\Http\ImmediateResponseException;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Imaging\IconFactory;
use TYPO3\CMS\Core\Type\ContextualFeedbackSeverity;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Http\ForwardResponse;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException;
use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;
use TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException;
use TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Scheduler\Task\AbstractTask;
use TYPO3\CMS\Scheduler\Task\TableGarbageCollectionTask;
use UnexpectedValueException;

/**
 * Route Controller
 */
class RouteController extends ActionController {
	/**
	 * The uid of the current root page
	 *
	 * @var int
	 */
	protected int $rootPageUid = 0;

	/**
	 * Command array on the form [tablename][uid][command] = value.
	 * This array may get additional data set internally based on clipboard commands send in CB var!
	 *
	 * @var array
	 */
	protected array $command;

	/**
	 * Clipboard command array. May trigger changes in "cmd"
	 *
	 * @var array
	 */
	protected array $clipboardCommandArray;

	/**
	 * DocHeaderComponent
	 *
	 * @var DocHeaderComponent
	 */
	protected DocHeaderComponent $docHeaderComponent;

	/**
	 * @var RouteRepository
	 */
	protected RouteRepository $routeRepository;

	/**
	 * @var CategoryRepository
	 */
	protected CategoryRepository $categoryRepository;

	/**
	 * @var LogRepository
	 */
	protected LogRepository $logRepository;

	/**
	 * @var ModuleTemplateFactory
	 */
	protected ModuleTemplateFactory $moduleTemplateFactory;

	/**
	 * @var ModuleTemplate
	 */
	protected ModuleTemplate $moduleTemplate;

	/**
	 * @var SchedulerTaskRepository
	 */
	protected SchedulerTaskRepository $schedulerTaskRepository;

	public function __construct(
		RouteRepository $routeRepository,
		CategoryRepository $categoryRepository,
		LogRepository $logRepository,
		ModuleTemplateFactory $moduleTemplateFactory,
		SchedulerTaskRepository $schedulerTaskRepository
	) {
		$this->routeRepository = $routeRepository;
		$this->categoryRepository = $categoryRepository;
		$this->logRepository = $logRepository;
		$this->moduleTemplateFactory = $moduleTemplateFactory;
		$this->schedulerTaskRepository = $schedulerTaskRepository;
	}

	/**
	 * Initialize action for all actions
	 *
	 * @throws Exception
	 * @throws ExtensionConfigurationExtensionNotConfiguredException
	 * @throws ExtensionConfigurationPathDoesNotExistException
	 */
	public function initializeAction(): void {
		$this->command = $this->request->getQueryParams()['cmd'] ?? [];
		$this->clipboardCommandArray = $this->request->getQueryParams()['CB'] ?? [];
		$this->initClipboard();

		// create doc header component
		$pageUid = $this->request->getQueryParams()['id'] ?? 0;
		/** @var BackendUserAuthentication $backendUser */
		$backendUser = $GLOBALS['BE_USER'];
		$pageInfo = BackendUtility::readPageAccess($pageUid, $backendUser->getPagePermsClause(1));

		$this->moduleTemplate = $this->moduleTemplateFactory->create($this->request);
		$this->docHeaderComponent = $this->moduleTemplate->getDocHeaderComponent();

		if ($pageUid) {
			$this->rootPageUid = RoutingService::getRootUidByPageUid($pageUid);
		}

		if (is_array($pageInfo)) {
			$this->docHeaderComponent->setMetaInformation($pageInfo);
		}
		// Check if Site is Root

		$this->makeButtons();
		$this->moduleTemplate->assign('pageUid', $pageUid);
		$this->moduleTemplate->assign('rootPageUid', $this->rootPageUid);
		$this->moduleTemplate->assign('docHeader', $this->docHeaderComponent->docHeaderContent());
		$this->switchActionMode();

		if (!$this->rootPageUid) {
			$connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
			$queryBuilder = $connectionPool->getQueryBuilderForTable('pages');
			$queryBuilder->getRestrictions()
				->removeAll()
				->add(GeneralUtility::makeInstance(WorkspaceRestriction::class))
				->add(GeneralUtility::makeInstance(DeletedRestriction::class));
			$rootOptionRows = $queryBuilder->select('*')
				->from('pages')
				->where(
					$queryBuilder->expr()->and(
						$queryBuilder->expr()->eq(
							'sys_language_uid',
							$queryBuilder->createNamedParameter(0, \PDO::PARAM_INT)
						),
						$queryBuilder->expr()->eq(
							'is_siteroot',
							$queryBuilder->createNamedParameter(1, \PDO::PARAM_INT)
						)
					)
				)->orderBy('sorting')->executeQuery()->fetchAllAssociative();
			$rootOptions = [];
			foreach ($rootOptionRows as $row) {
				$pageInfo = BackendUtility::readPageAccess($row['uid'], $GLOBALS['BE_USER']->getPagePermsClause(1));
				if ($pageInfo) {
					$rootline = BackendUtility::BEgetRootLine($pageInfo['uid'], '', TRUE);
					\ksort($rootline);
					$path = '/root';
					foreach ($rootline as $page) {
						$path .= '/p' . \dechex($page['uid']);
					}

					$pageInfo['path'] = $path;
					$pageInfo['_thePathFull'] = \substr($pageInfo['_thePathFull'], 1);
					$pageInfo['_thePathFull'] = \substr($pageInfo['_thePathFull'], 0, -1);
					$rootOptions[] = $pageInfo;
				}
			}

			$this->moduleTemplate->assign('rootOptions', $rootOptions);
		}

		if (!LicenceCheckService::hasValidLicense()) {
			$this->moduleTemplate->assign('showLicenseBanner', TRUE);
		}

		// show warning, if there is no TableGarbageCollectionTask configured for the table: 'tx_sgroutes_domain_model_routehit'
		if (!$this->isGarbageCollectionTaskSetUpForRoutehits()) {
			$this->addFlashMessage(
				LocalizationUtility::translate('backend.table_gc_task_not_set_up', 'SgRoutes'),
				LocalizationUtility::translate('backend.table_gc_task_not_set_up.title', 'SgRoutes'),
				ContextualFeedbackSeverity::ERROR
			);
		}

		$this->moduleTemplate->assign('action', $this->request->getControllerActionName());
		$this->initDemoMode();
	}

	/**
	 * Shows all the data for the selected page browser page in the backend.
	 *
	 * @param array|null $filters
	 * @param int $currentPage
	 * @return ResponseInterface
	 * @throws InvalidQueryException
	 */
	public function listAction(array $filters = NULL, int $currentPage = 1): ResponseInterface {
		$pageUid = (int) ($this->request->getQueryParams()['id'] ?? 0);

		// Check specifically for website Page 0
		$isSiteRoot = ($pageUid === 0);

		if ($this->rootPageUid) {
			/** @var BackendUserAuthentication $backendUser */
			$backendUser = $GLOBALS['BE_USER'];
			if ($filters === NULL) {
				$filters = $backendUser->getModuleData('web_SgRoutesRoute_filters', 'ses') ?: [];
			} else {
				$backendUser->pushModuleData('web_SgRoutesRoute_filters', $filters);
			}

			$routes = $this->routeRepository->findRoutes($this->rootPageUid, $filters);
			$pagination = GeneralUtility::makeInstance(Pagination::class, $routes, $currentPage, 20);
			$this->moduleTemplate->assign('pagination', $pagination);

			$prevUid = 0;
			$prevPrevUid = 0;
			$sortingData = [];
			foreach ($routes as $route) {
				if ($prevUid) {
					$sortingData['prev'][$route->getUid()] = $prevPrevUid;
					$sortingData['next'][$prevUid] = '-' . $route->getUid();
					$sortingData['prevUid'][$route->getUid()] = $prevUid;
				}

				$prevPrevUid = isset($sortingData['prev'][$route->getUid()]) ? -$prevUid : $route->getPid();
				$prevUid = $route->getUid();
			}

			/** @noinspection PhpUndefinedMethodInspection */
			/** @var QueryResultInterface $categories */
			$categories = $this->categoryRepository->findByPid($this->rootPageUid);
			$categoryOptions = [];
			foreach ($categories as $category) {
				$categoryOptions[$category->getUid()] = $category->getTitle();
			}

			$this->moduleTemplate->assign('sortingData', $sortingData);
			$this->moduleTemplate->assign('categoryOptions', $categoryOptions);
			$this->moduleTemplate->assign('filters', $filters);
		}

		$this->moduleTemplate->assign('useEmptyLayout', $isSiteRoot);

		return $this->moduleTemplate->renderResponse('List');
	}

	/**
	 * create buttons for the backend module header
	 *
	 * @return void
	 * @throws InvalidArgumentException
	 * @throws UnexpectedValueException
	 */
	private function makeButtons() {
		$buttonBar = $this->docHeaderComponent->getButtonBar();

		/** @var IconFactory $iconFactory */
		$iconFactory = GeneralUtility::makeInstance(IconFactory::class);

		// Refresh
		$refreshButton = $buttonBar->makeLinkButton()
			->setHref(GeneralUtility::getIndpEnv('REQUEST_URI'))
			->setTitle(
				LocalizationUtility::translate(
					'LLL:EXT:core/Resources/Private/Language/locallang_core.xlf:labels.reload'
				)
			)
			->setIcon($iconFactory->getIcon('actions-refresh', Icon::SIZE_SMALL));
		$buttonBar->addButton($refreshButton, ButtonBar::BUTTON_POSITION_RIGHT);

		// shortcut button
		$shortcutButton = $buttonBar->makeShortcutButton()
			->setDisplayName('Redirect')
			->setRouteIdentifier($this->request->getPluginName())
			->setArguments(
				[
					'id' => [],
					'M' => []
				]
			);

		$buttonBar->addButton($shortcutButton, ButtonBar::BUTTON_POSITION_RIGHT);
	}

	/**
	 * Delete all Routes
	 *
	 * @param array $filters
	 * @return ResponseInterface
	 * @throws IllegalObjectTypeException
	 * @throws InvalidQueryException
	 */
	public function deleteAllAction(array $filters = []): ResponseInterface {
		/** @var ObjectStorage $routes */
		$routes = $this->routeRepository->findRoutes($this->rootPageUid, $filters);
		if ($routes && $routesCount = $routes->count()) {
			foreach ($routes as $route) {
				$this->routeRepository->remove($route);
			}

			$message = LocalizationUtility::translate('backend.delete_success', 'sg_routes', [$routesCount]);
			$this->addFlashMessage($message);
		} else {
			$message = LocalizationUtility::translate('backend.noRoutesMessage', 'sg_routes');
			$this->addFlashMessage($message, '', ContextualFeedbackSeverity::ERROR);
		}

		return $this->redirect('list');
	}

	/**
	 * Reads from the backend user session
	 */
	protected function getFromSession(string $key): ?string {
		return $GLOBALS['BE_USER']->getSessionData('sg_routes_' . $key) ?? NULL;
	}

	/**
	 * Writes to the backend user session
	 */
	protected function writeToSession(string $key, $value): void {
		$GLOBALS['BE_USER']->setAndSaveSessionData('sg_routes_' . $key, $value);
	}

	protected function switchActionMode(): void {
		// 1) Which action am I currently running?
		$currentAction = $this->request->getControllerActionName();

		// 2) Read query params
		$queryParams = $this->request->getQueryParams();

		// 3) If user just clicked the menu item, we see lastAction in the parameters
		$mode = $queryParams['parameters']['lastAction'] ?? $this->getFromSession('mode');

		if (!empty($mode)) {
			// Store it in the session
			$this->writeToSession('mode', $mode);

			// 4) Optionally skip if page=0 (only do redirect if user is on a real page)
			$pageUid = (int) ($queryParams['id'] ?? 0);
			// e.g. if you do *NOT* want to skip, remove this condition:
			if ($pageUid > 0 && $mode !== $currentAction) {
				// The user last used a different action => redirect
				$redirectResponse = $this->redirect($mode);  // e.g. $this->redirect('list') or 'log'
				throw new ImmediateResponseException($redirectResponse);
			}
		}
	}

	/**
	 * Show htaccess Strings
	 *
	 * @param array $filters
	 * @return ResponseInterface|null
	 * @throws InvalidQueryException
	 */
	public function htaccessAction(array $filters = []): ?ResponseInterface {
		/** @var array $routes */
		$routes = $this->routeRepository->findRoutes($this->rootPageUid, $filters, TRUE);
		$routingService = GeneralUtility::makeInstance(RoutingService::class);
		$data = $routingService->getHtaccessEntries($routes);

		$this->moduleTemplate->assign('data', $data);

		return $this->moduleTemplate->renderResponse('Htaccess');
	}

	/**
	 * Shows all log data of the executed redirects.
	 *
	 * @param int $currentPage
	 * @return ResponseInterface|null
	 */
	public function logAction(int $currentPage = 1): ?ResponseInterface {
		if ($this->rootPageUid) {
			$queryResult = $this->logRepository->findAllByPid($this->rootPageUid);
			$pagination = GeneralUtility::makeInstance(Pagination::class, $queryResult, $currentPage, 50);
			$this->moduleTemplate->assign('pagination', $pagination);
		}

		return $this->moduleTemplate->renderResponse('Log');
	}

	/**
	 * Export the route results to a .csv file
	 *
	 * @param array $filters
	 * @return ResponseInterface
	 * @throws InvalidQueryException
	 * @throws Exception
	 * @throws \TYPO3\CMS\Extbase\Persistence\Generic\Exception
	 */
	public function exportAction(array $filters = []): ResponseInterface {
		$exportString = '';
		if ($this->rootPageUid) {
			/** @var ObjectStorage $routes */
			$routes = $this->routeRepository->findRoutes($this->rootPageUid, $filters);
			$export = [[]];
			$dataMapper = GeneralUtility::makeInstance(DataMapper::class);
			$dataMap = $dataMapper->getDataMap(Route::class);
			$emptyRoute = GeneralUtility::makeInstance(Route::class);
			$propertyColumns = ['uid' => 'uid', 'pid' => 'pid'];
			foreach ($emptyRoute->_getProperties() as $propertyName => $propertyValue) {
				$columnMap = $dataMap->getColumnMap($propertyName);
				if ($columnMap) {
					$propertyColumns[$propertyName] = $columnMap->getColumnName();
				}
			}

			$dateFields = ['tstamp', 'crdate'];
			foreach ($propertyColumns as $propertyName => $columnName) {
				$label = isset($GLOBALS['TCA']['sys_redirect']['columns'][$columnName]) ?
					$GLOBALS['TCA']['sys_redirect']['columns'][$columnName]['label'] : '';
				if (str_starts_with($label, 'LLL:')) {
					$label = $GLOBALS['LANG']->sL($label);
				}

				$export[0][] = $label ?: $propertyName;
			}

			if ($routes && $routes->count()) {
				$line = 1;
				/** @var Route $route */
				foreach ($routes as $route) {
					foreach ($propertyColumns as $propertyName => $columnName) {
						$value = $route->_getProperty($propertyName);
						if (\in_array($propertyName, $dateFields, TRUE)) {
							$export[$line][] = $value ? \date('d.m.Y', $value) : '';
						} elseif ($propertyName === 'categories') {
							$categoryString = '';
							if ($value) {
								/** @var Category $category */
								foreach ($value as $category) {
									if ($categoryString !== '') {
										$categoryString .= ', ';
									}

									$categoryString .= $category->getTitle();
								}
							}

							$export[$line][] = $categoryString;
						} else {
							$export[$line][] = $value;
						}
					}

					$line++;
				}
			}

			foreach ($export as $line) {
				/** @var array $line */
				$fields = [];
				foreach ($line as $field) {
					$fields[] = '"' . $field . '"';
				}

				$exportString .= \implode(',', $fields) . LF;
			}
		}

		\header('Content-Type: application/force-download');
		\header('Content-Transfer-Encoding: Binary');
		\header('Content-Disposition: attachment; filename="export.csv"');
		\header('Content-Length: ' . \strlen($exportString));
		echo $exportString;
		exit(0);
	}

	/**
	 * Clipboard pasting and deleting.
	 *
	 * @throws InvalidArgumentException
	 */
	public function initClipboard(): void {
		if (\is_array($this->clipboardCommandArray)) {
			$clipObj = GeneralUtility::makeInstance(Clipboard::class);
			$clipObj->initializeClipboard();
			if (isset($this->clipboardCommandArray['paste']) && $this->clipboardCommandArray['paste']) {
				$clipObj->setCurrentPad($this->clipboardCommandArray['pad']);
				$this->command = $this->makePasteCmdArray(
					$this->clipboardCommandArray['paste'],
					$this->command,
					$this->clipboardCommandArray['update'] ?? NULL,
					$clipObj
				);
			}

			if (isset($this->clipboardCommandArray['delete']) && $this->clipboardCommandArray['delete']) {
				$clipObj->setCurrentPad($this->clipboardCommandArray['pad']);
				$this->command = $this->makeDeleteCmdArray($this->command, $clipObj);
			}

			if (isset($this->clipboardCommandArray['el']) && $this->clipboardCommandArray['el']) {
				$this->clipboardCommandArray['setP'] = 'normal';
				$clipObj->setCmd($this->clipboardCommandArray);
				$clipObj->cleanCurrent();
				$clipObj->endClipboard();
			}
		}
	}

	/**
	 * Resets the hits for a route & forwards the user to the list action afterward
	 *
	 * @param int $routeUid
	 * @return ResponseInterface
	 * @throws IllegalObjectTypeException
	 * @throws UnknownObjectException
	 */
	public function resetHitsAction(int $routeUid): ResponseInterface {
		$this->routeRepository->resetHitCountForRoute($routeUid);
		return new ForwardResponse('list');
	}

	/**
	 * Fetches available scheduler tasks and filters them (first by class, then by the table argument).
	 * We do this, to be able to show a warning flash message to the user, in case the task is not set up.
	 *
	 * @return bool
	 * @throws Exception
	 */
	protected function isGarbageCollectionTaskSetUpForRoutehits(): bool {
		$taskExists = FALSE;
		// fetch all scheduler tasks
		$allTasks = $this->schedulerTaskRepository->fetchTasksWithCondition('', TRUE);
		$allTableGarbageCollectionTasks = [];
		/** @var AbstractTask $aTaskObject */
		foreach ($allTasks as $aTaskObject) {
			// skip tasks, that are not of class TableGarbageCollectionTask
			if (get_class($aTaskObject) === TableGarbageCollectionTask::class) {
				$allTableGarbageCollectionTasks[] = $aTaskObject;
			}
		}

		/** @var TableGarbageCollectionTask $garbageCollectionTask */
		foreach ($allTableGarbageCollectionTasks as $garbageCollectionTask) {
			// we found the right task, now check if the table is set to the route hit table OR if allTables is checked
			if ($garbageCollectionTask->allTables || $garbageCollectionTask->table === RoutehitRepository::TABLE_NAME) {
				$taskExists = TRUE;
			}
		}

		return $taskExists;
	}

	/**
	 * Initializes the demo mode
	 */
	protected function initDemoMode() {
		$keyState = LicenceCheckService::checkKey();
		$hasValidLicense = LicenceCheckService::hasValidLicense();
		$isInDemoMode = LicenceCheckService::isInDemoMode();
		if ($isInDemoMode && !$hasValidLicense) {
			// - 1 because the flash message would show 00:00:00 instead of 23:59:59
			$description = LocalizationUtility::translate(
				'backend.licenseKey.isInDemoMode.description',
				'sg_routes',
				[
					date('H:i:s', mktime(0, 0, LicenceCheckService::getRemainingTimeInDemoMode() - 1))
				]
			);
			$this->addFlashMessage(
				$description,
				LocalizationUtility::translate('backend.licenseKey.isInDemoMode.header', 'sg_routes'),
				ContextualFeedbackSeverity::INFO
			);
		} elseif ($keyState === LicenceCheckService::STATE_LICENSE_NOT_SET) {
			$description = LocalizationUtility::translate(
				'backend.licenseKey.notSet.descriptionTYPO3-9',
				'sg_routes'
			);

			if (LicenceCheckService::isInDevelopmentContext()) {
				$description .= ' ' . LocalizationUtility::translate(
					'backend.licenseKey.error.dev',
					'sg_routes'
				);
			}

			$this->addFlashMessage(
				$description,
				LocalizationUtility::translate('backend.licenseKey.notSet.header', 'sg_routes'),
				ContextualFeedbackSeverity::WARNING
			);
		} elseif (!$hasValidLicense) {
			$description = LocalizationUtility::translate(
				'backend.licenseKey.invalid.descriptionTYPO3-9',
				'sg_routes'
			);

			if (LicenceCheckService::isInDevelopmentContext()) {
				$description .= ' ' . LocalizationUtility::translate(
					'backend.licenseKey.error.dev',
					'sg_routes'
				);
			}

			$this->addFlashMessage(
				$description,
				LocalizationUtility::translate('backend.licenseKey.invalid.header', 'sg_routes'),
				ContextualFeedbackSeverity::ERROR
			);
		}

		$this->moduleTemplate->assign('invalidKey', !$hasValidLicense);
		$this->moduleTemplate->assign('showDemoButton', !$isInDemoMode && LicenceCheckService::isDemoModeAcceptable());
	}

	/**
	 * Activates the demo mode for the given instance.
	 *
	 * @throws AspectNotFoundException
	 */
	public function activateDemoModeAction(): ResponseInterface {
		if (LicenceCheckService::isInDemoMode() || !LicenceCheckService::isDemoModeAcceptable()) {
			return $this->redirect('list');
		}

		LicenceCheckService::activateDemoMode();

		return $this->redirect('list');
	}

	/**
	 * Backported removed makePasteCmdArray function for TYPO3 v11
	 * Only changed some calls from $this to $clipObj́
	 *
	 * @param string $ref
	 * @param array $CMD
	 * @param array|null $update
	 * @return array
	 */
	public function makePasteCmdArray($ref, $CMD, array $update = NULL, $clipBoard) {
		[$pTable, $pUid] = explode('|', $ref);
		$pUid = (int) $pUid;
		// pUid must be set and if pTable is not set (that means paste ALL elements)
		// the uid MUST be positive/zero (pointing to page id)
		if ($pTable || $pUid >= 0) {
			$elements = $clipBoard->elFromTable($pTable);
			// So the order is preserved.
			$elements = array_reverse($elements);
			$mode = $clipBoard->currentMode() === 'copy' ? 'copy' : 'move';
			// Traverse elements and make CMD array
			foreach ($elements as $tP => $value) {
				[$table, $uid] = explode('|', $tP);
				if (!is_array($CMD[$table])) {
					$CMD[$table] = [];
				}

				if (is_array($update)) {
					$CMD[$table][$uid][$mode] = [
						'action' => 'paste',
						'target' => $pUid,
						'update' => $update,
					];
				} else {
					$CMD[$table][$uid][$mode] = $pUid;
				}

				if ($mode === 'move') {
					$clipBoard->removeElement($tP);
				}
			}

			$clipBoard->endClipboard();
		}

		return $CMD;
	}

	/**
	 * Backported removed makeDeleteCmdArray function for TYPO3 v11
	 * Only changed some calls from $this to $clipObj́
	 *
	 * @param array $CMD
	 * @return array
	 */
	public function makeDeleteCmdArray($CMD, $clipBoard) {
		// all records
		$elements = $clipBoard->elFromTable('');
		foreach ($elements as $tP => $value) {
			[$table, $uid] = explode('|', $tP);
			if (!is_array($CMD[$table])) {
				$CMD[$table] = [];
			}

			$CMD[$table][$uid]['delete'] = 1;
			$clipBoard->removeElement($tP);
		}

		$clipBoard->endClipboard();
		return $CMD;
	}
}
