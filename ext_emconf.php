<?php

$EM_CONF['sg_routes'] = [
	'title' => 'URL Redirects and Page Not Found Handling',
	'description' => 'URL Redirects and Page Not Found Handling',
	'category' => 'module',
	'version' => '7.1.0',
	'state' => 'stable',
	'author' => 'Stefan Galinski',
	'author_email' => 'stefan@sgalinski.de',
	'author_company' => 'sgalinski Internet Services (https://www.sgalinski.de)',
	'constraints' => [
		'depends' => [
			'typo3' => '12.4.0-12.4.99',
			'php' => '8.1.0-8.3.99',
		],
		'conflicts' => [
		],
		'suggests' => [
			'sg_account' => ''
		],
	],
];
