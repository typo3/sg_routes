# Ext: sg_routes

<img src="https://www.sgalinski.de/typo3conf/ext/project_theme/Resources/Public/Images/logo.svg" />

License: [GNU GPL, Version 2](https://www.gnu.org/licenses/gpl-2.0.html)

Repository: https://gitlab.sgalinski.de/typo3/sg_routes

Please report bugs here: https://gitlab.sgalinski.de/typo3/sg_routes

**Note**: sg_routes can be used for free or by buying a license key from
our [online shop](https://shop.sgalinski.de/products/).
You can enter the license key in the extension manager configuration of sg_routes.

Features excluded from the free version:

* Regular expression routing
* .htaccess Redirect rules generation
* The Redirect Log view

## About

With this extension you can simply create and manage URL redirects for your page and generate redirect rules for your
.htaccess file.

## Important note!

sg_routes is a multi-domain extension so before you use the configuration module, make sure that all the domains
in your installation have correctly configured domain records for their site-root page.

You can find more
information [here](https://docs.typo3.org/typo3cms/extensions/crystalis/LanguageService/HandlingDomains/Index.html).

## The Backend Module

After a successful Installation, you have a new module in the "WEB" section of your TYPO3 Backend.

Clicking on it loads the administration panel. Using the drop-down above you can switch between the 'Redirect
Management', 'Redirect Log' and the 'Page Not Found Handling' views.

### The Redirect Management view

Here, all your custom URL routes are listed. The list can be filtered by `Categories` and search words. You can also
create, update and delete your routes and categories.

**Note**: To create a route/category, you need to first select a page in the page tree.

### The Redirect Log view

In this view, you can find logs of when your route records were used to redirect the users of your website, and also the
time it took for each redirect to execute (in seconds).

**Note**: Depending on how often the redirects are triggered, the log database table can get pretty large. To handle
this,
you will find a scheduler 'garbage collection' task registered for the `tx_sgroutes_domain_model_log` table. It is
recommeded
to set up this task after you've installed the extension and make sure that it is periodically triggered by the
scheduler.
If you haven't done so already, you can find more info about how to set up the cron job for the scheduler extension by
following this [link](https://docs.typo3.org/typo3cms/extensions/scheduler/Installation/CronJob/Index.html)

## Usage

### Creating a redirect rule

In your backend module, click on
the  <img height="20px"  width="20px" src="https://camo.githubusercontent.com/cd0d2f12f28169acfbafd1c8bf8f291155f3d210/68747470733a2f2f7261776769742e636f6d2f5459504f332f5459504f332e49636f6e732f6d61737465722f646973742f616374696f6e732f616374696f6e732d646f63756d656e742d6e65772e737667" />
"Create" button. Now enter from which url(**Source URL**) you want to redirect to (**Destination URL**) and whether you
want it to be a permanent (**301**) or a temporary redirect (**302**).

**Example**: You want to redirect temporarily from your starting page to the contact page

```
Source URL : /

Destination : /contact

Redirect Code: 302

Description: Here you can add a custom description for your rule, visible in the backend overview
```

When creating/editing a rule, you have also the option to `Use regular expression`. By checking this, the `Source URL`
is evaluated as a regular expression
and is matched against the request url. If a match is found and, the rule is applied with a rewrite based on
the `Destination URL`.

**Example**: You want to redirect permanently from your `/news/...` pages to `/blog/...`

```
Source URL : ^/news/(.+?)$

Destination : /blog/$1

Redirect Code: 301
```

**Note**: The rules are evaluated in the order you see them in the BE module list. Take this into account when creating
new rules, and that
you can adust the order of the rules by using the
<img height="20px" width="20px" src="https://camo.githubusercontent.com/44837146daee9155a37d1b899747d1bceaeecb5a/68747470733a2f2f7261776769742e636f6d2f5459504f332f5459504f332e49636f6e732f6d61737465722f646973742f616374696f6e732f616374696f6e732d6d6f76652d75702e737667" />,
<img height="20px" width="20px" src="https://camo.githubusercontent.com/3a2802e602e3d2a0039b79c9908ed7bca22d0347/68747470733a2f2f7261776769742e636f6d2f5459504f332f5459504f332e49636f6e732f6d61737465722f646973742f616374696f6e732f616374696f6e732d6d6f76652d646f776e2e737667" />,
<img height="20px" width="20px" src="https://camo.githubusercontent.com/1fc865a03039be5039e6069f2f53d1db2a8e3896/68747470733a2f2f7261776769742e636f6d2f5459504f332f5459504f332e49636f6e732f6d61737465722f646973742f616374696f6e732f616374696f6e732d656469742d6375742e737667" />
buttons

### Options for your redirect rule

After you created one or more redirect rules, you get a list overview in your backend module of all rules.

You can click on a rule to edit it or use the menu buttons:

<img height="20px" width="20px" src="https://camo.githubusercontent.com/e3de477db4caba47f3adc70db73b401be474ec23/68747470733a2f2f7261776769742e636f6d2f5459504f332f5459504f332e49636f6e732f6d61737465722f646973742f616374696f6e732f616374696f6e732d6f70656e2e737667"> Edit
<br>
<img height="20px" width="20px" src="https://camo.githubusercontent.com/3c5e6daff1f31fd3c4bf0ac3e70520133d06441c/68747470733a2f2f7261776769742e636f6d2f5459504f332f5459504f332e49636f6e732f6d61737465722f646973742f616374696f6e732f616374696f6e732d656469742d686964652e737667"> Disable/Enable
<br>
<img height="20px" width="20px" src="https://camo.githubusercontent.com/7773a0fc11517dc70b81f9ba516991f3669493e1/68747470733a2f2f7261776769742e636f6d2f5459504f332f5459504f332e49636f6e732f6d61737465722f646973742f616374696f6e732f616374696f6e732d656469742d64656c6574652e737667"> Delete
<br>
 .... Expand/Collapse the options menu
<br>
<img height="20px" width="20px" src="https://camo.githubusercontent.com/ee057cb37045beeccf8078f74e65e1774ec5e001/68747470733a2f2f7261776769742e636f6d2f5459504f332f5459504f332e49636f6e732f6d61737465722f646973742f616374696f6e732f616374696f6e732d646f63756d656e742d696e666f2e737667"> Show further information
<br>
<img height="20px" width="20px" src="https://camo.githubusercontent.com/91c383d7beded93dbe6a62e2a1ae94bf82d1d783/68747470733a2f2f7261776769742e636f6d2f5459504f332f5459504f332e49636f6e732f6d61737465722f646973742f616374696f6e732f616374696f6e732d646f63756d656e742d686973746f72792d6f70656e2e737667"> Show history
<br>
<img height="20px" width="20px" src="https://camo.githubusercontent.com/cf8d1359b3a6ef79ca3731f6f0e0e01bcf992dcb/68747470733a2f2f7261776769742e636f6d2f5459504f332f5459504f332e49636f6e732f6d61737465722f646973742f616374696f6e732f616374696f6e732d76657273696f6e2d706167652d6f70656e2e737667"> Versioning
<br>
<img height="20px" width="20px" src="https://camo.githubusercontent.com/dd099d0fe1ea64df34ba6c256d05962e5e11d7a9/68747470733a2f2f7261776769742e636f6d2f5459504f332f5459504f332e49636f6e732f6d61737465722f646973742f616374696f6e732f616374696f6e732d6164642e737667"> Create new
<br>
<img height="20px" width="20px" src="https://camo.githubusercontent.com/44837146daee9155a37d1b899747d1bceaeecb5a/68747470733a2f2f7261776769742e636f6d2f5459504f332f5459504f332e49636f6e732f6d61737465722f646973742f616374696f6e732f616374696f6e732d6d6f76652d75702e737667"> Move up
<br>
<img height="20px" width="20px" src="https://camo.githubusercontent.com/3a2802e602e3d2a0039b79c9908ed7bca22d0347/68747470733a2f2f7261776769742e636f6d2f5459504f332f5459504f332e49636f6e732f6d61737465722f646973742f616374696f6e732f616374696f6e732d6d6f76652d646f776e2e737667"> Move down
<br>
<img height="20px" width="20px" src="https://camo.githubusercontent.com/1794befd17574f066d0b3513729d731c158b8b0a/68747470733a2f2f7261776769742e636f6d2f5459504f332f5459504f332e49636f6e732f6d61737465722f646973742f616374696f6e732f616374696f6e732d656469742d636f70792e737667"> Copy
<br>
<img height="20px" width="20px" src="https://camo.githubusercontent.com/1fc865a03039be5039e6069f2f53d1db2a8e3896/68747470733a2f2f7261776769742e636f6d2f5459504f332f5459504f332e49636f6e732f6d61737465722f646973742f616374696f6e732f616374696f6e732d656469742d6375742e737667"> Cut
<br>
<img height="20px" width="20px" src="https://camo.githubusercontent.com/7908ba1757d3b77ba3b9444c095bb0582e74a4af/68747470733a2f2f7261776769742e636f6d2f5459504f332f5459504f332e49636f6e732f6d61737465722f646973742f616374696f6e732f616374696f6e732d646f63756d656e742d70617374652d61667465722e737667"> Paste after

### Export your redirect rule(s) to your .htaccess file

It is a good practice to insert your redirect routes into your .htaccess file for better performance.

Simply click
the <img height="20px" width="20px" src="https://camo.githubusercontent.com/4b1188209e740e17a4ec0cd6583425696809017b/68747470733a2f2f7261776769742e636f6d2f5459504f332f5459504f332e49636f6e732f6d61737465722f646973742f616374696f6e732f616374696f6e732d646f63756d656e742d766965772e737667" /> "
htaccess" button and copy & paste the textarea content into your .htaccess file.

If all works, you can now delete your routes with
the <img height="20px" width="20px" src="https://camo.githubusercontent.com/7773a0fc11517dc70b81f9ba516991f3669493e1/68747470733a2f2f7261776769742e636f6d2f5459504f332f5459504f332e49636f6e732f6d61737465722f646973742f616374696f6e732f616374696f6e732d656469742d64656c6574652e737667" /> "
Delete all" Button.

**Note**: Be aware that the "htaccess" and "Delete all" actions will for the current rule filter options. So only the
rules you can see
in the paginated list will be taken into account.

## Developer Guide

### TYPO3 Bootstrap Hook

To have the best Performance, this extension hooks into the TYPO3 bootstrap mechanism as early as possible, after the
domain root-page is identified.

This is done inside the ext_localconf.php with a configuration array by supplying a path to the desired hook
function `SGalinski\SgRoutes\Service\RoutingService->hook`.
By extending or replacing this function, it is possible to implement more complex redirect / rewrite rules.

You can find more information on hooks in
the [TYPO3 Documentation](https://docs.typo3.org/typo3cms/CoreApiReference/ApiOverview/Hooks/Configuration/Index.html).

## FAQ

### Why should I put my redirect rules into the .htaccess file?

You get a slightly better performance for your website by doing this.

### I get the error "Too many redirects" when trying to open my website

You are most likely stuck in a *redirect* loop. Backup your routes by copying up your .htaccess entries and reset all
your changes.
Try to determine if you have created an endless loop with your routes, for instance:

```
/home redirects to /contact
/contact redirects to /support
/support redirects to /home
```
